SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `innotrace` ;
CREATE SCHEMA IF NOT EXISTS `innotrace` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `innotrace` ;

-- -----------------------------------------------------
-- Table `innotrace`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`users` (
  `username` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `role` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`username`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `innotrace`.`activities`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`activities` (
  `id` VARCHAR(255) NOT NULL ,
  `type` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `date` DATE NOT NULL ,
  `instructions` TEXT NULL ,
  `timeout` BIGINT NOT NULL ,
  `owner` VARCHAR(255) NOT NULL ,
  `recordscreen` TINYINT(1) NOT NULL ,
  `recordgaze` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_activity_owner` (`owner` ASC) ,
  CONSTRAINT `fk_activity_owner`
    FOREIGN KEY (`owner` )
    REFERENCES `innotrace`.`users` (`username` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `innotrace`.`activity_subjects`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`activity_subjects` (
  `activity` VARCHAR(255) NOT NULL ,
  `subject` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`activity`, `subject`) ,
  INDEX `fk_activity_subjects_activity` (`activity` ASC) ,
  INDEX `fk_activity_subjects_subject` (`subject` ASC) ,
  CONSTRAINT `fk_activity_subjects_activity`
    FOREIGN KEY (`activity` )
    REFERENCES `innotrace`.`activities` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_activity_subjects_subject`
    FOREIGN KEY (`subject` )
    REFERENCES `innotrace`.`users` (`username` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `innotrace`.`pictureactivities`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`pictureactivities` (
  `id` VARCHAR(255) NOT NULL ,
  `image` LONGBLOB NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_pictureactivities_activity` (`id` ASC) ,
  CONSTRAINT `fk_pictureactivities_activity`
    FOREIGN KEY (`id` )
    REFERENCES `innotrace`.`activities` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `innotrace`.`observations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`observations` (
  `activity` VARCHAR(255) NOT NULL ,
  `subject` VARCHAR(255) NOT NULL ,
  `cancellation` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`activity`, `subject`) ,
  INDEX `fk_observations_activity` (`activity` ASC) ,
  INDEX `fk_observations_subject` (`subject` ASC) ,
  CONSTRAINT `fk_observations_activity`
    FOREIGN KEY (`activity` )
    REFERENCES `innotrace`.`activities` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_observations_subject`
    FOREIGN KEY (`subject` )
    REFERENCES `innotrace`.`users` (`username` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `innotrace`.`recordings`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `innotrace`.`recordings` (
  `activity` VARCHAR(255) NOT NULL ,
  `subject` VARCHAR(255) NOT NULL ,
  `timestamp` BIGINT NOT NULL ,
  `gazeposition` BLOB NULL ,
  `screenshot` LONGBLOB NULL ,
  PRIMARY KEY (`activity`, `subject`, `timestamp`) ,
  INDEX `fk_recordings_activity` (`activity` ASC) ,
  INDEX `fk_recordings_subject` (`subject` ASC) ,
  CONSTRAINT `fk_recordings_activity`
    FOREIGN KEY (`activity` )
    REFERENCES `innotrace`.`activities` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_recordings_subject`
    FOREIGN KEY (`subject` )
    REFERENCES `innotrace`.`users` (`username` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `innotrace`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `innotrace`;
INSERT INTO `innotrace`.`users` (`username`, `name`, `password`, `role`) VALUES ('jonathan', 'Jonathan Frank', '', 'ADMIN');

COMMIT;

-- -----------------------------------------------------
-- Data for table `innotrace`.`activities`
-- -----------------------------------------------------
START TRANSACTION;
USE `innotrace`;
INSERT INTO `innotrace`.`activities` (`id`, `type`, `name`, `date`, `instructions`, `timeout`, `owner`, `recordscreen`, `recordgaze`) VALUES ('51c80bb0-c30e-11e3-8a33-0800200c9a66', 'com.innobox.innotrace.data.PictureActivity', 'Test 1', '2014-01-01', '', 3, 'jonathan', 1, 0);

COMMIT;

-- -----------------------------------------------------
-- Data for table `innotrace`.`activity_subjects`
-- -----------------------------------------------------
START TRANSACTION;
USE `innotrace`;
INSERT INTO `innotrace`.`activity_subjects` (`activity`, `subject`) VALUES ('51c80bb0-c30e-11e3-8a33-0800200c9a66', 'jonathan');

COMMIT;

-- -----------------------------------------------------
-- Data for table `innotrace`.`pictureactivities`
-- -----------------------------------------------------
START TRANSACTION;
USE `innotrace`;
INSERT INTO `innotrace`.`pictureactivities` (`id`, `image`) VALUES ('51c80bb0-c30e-11e3-8a33-0800200c9a66', NULL);

COMMIT;
