/**
 * File:		ObservationDAOImpl.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 17.13.39
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.awt.Point;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.rowset.CachedRowSet;

import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.IObservationDAO;
import com.innobox.innotrace.data.Observation;
import com.innobox.innotrace.data.User;
import com.innobox.innotrace.util.Image;
import com.innobox.innotrace.util.ObjectSerializer;

/**
 * @author jonathanfrank
 * 
 */
public class ObservationDAOImpl extends UnicastRemoteObject implements IObservationDAO {
	
	private transient RMIServer server;
	private transient IDataAccessService dataAccessService;
	
	/**
	 * @param server
	 * @param dataAccessService
	 */
	public ObservationDAOImpl(RMIServer server, IDataAccessService dataAccessService) throws RemoteException {
		this.server = server;
		this.dataAccessService = dataAccessService;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IObservationDAO#getObservationsByActivity(java.util.UUID)
	 */
	@Override
	public List<Observation> getObservationsByActivity(UUID activityID) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select * from observations where activity = ?", activityID.toString());
			
			ArrayList<Observation> list = new ArrayList<>();
			
			while (rowSet.next()) {
				Observation o = new Observation(UUID.fromString(rowSet.getString("activity")), rowSet.getString("subject"), rowSet.getBoolean("cancellation"), null);
				o.setRecordings(fetchRecordings(activityID, rowSet.getString("subject")));
				list.add(o);
			}
			
			return list;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IObservationDAO#getObservation(java.util.UUID,
	 *      java.lang.String)
	 */
	@Override
	public Observation getObservation(UUID activityID, String subjectUsername) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select * from observations where activity = ? and subject = ?", activityID.toString(), subjectUsername);
			
			if (rowSet.next()) {
				Observation o = new Observation(UUID.fromString(rowSet.getString("activity")), rowSet.getString("subject"), rowSet.getBoolean("cancellation"), null);
				o.setRecordings(fetchRecordings(activityID, subjectUsername));
				return o;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IObservationDAO#addObservation(com.innobox.innotrace.data.Observation)
	 */
	@Override
	public void addObservation(Observation o) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !(dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN) || dataAccessService.getCurrentUser().getRole().equals(User.Role.EXPERIMENTER) || dataAccessService.getCurrentUser().getRole().equals(User.Role.SUBJECT))) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at oprette en observation");
		}
		
		try {
			server.getConnection().executeUpdate("insert into observations (activity, subject, cancellation) values (?, ?, ?)", o.getActivityID().toString(), o.getSubject(), o.isCancellation());
			
			if (o.getRecordings() != null) {
				for (Map.Entry<Long, Observation.Recording> entry : o.getRecordings().entrySet()) {
					server.getConnection().executeUpdate("insert into recordings (activity, subject, timestamp, gazeposition, screenshot) values (?, ?, ?, ?, ?)", o.getActivityID().toString(), o.getSubject(), entry.getKey(), ObjectSerializer.serializeObject(entry.getValue().getGazePosition()), ObjectSerializer.serializeObject(entry.getValue().getScreenshot()));
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IObservationDAO#removeObservation(java.util.UUID,
	 *      java.lang.String)
	 */
	@Override
	public void removeObservation(UUID activityID, String subjectUsername) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !(dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN) || dataAccessService.getCurrentUser().getRole().equals(User.Role.EXPERIMENTER))) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at slette en observation");
		}
		
		try {
			server.getConnection().executeUpdate("delete from observations where activity = ? and subject = ?", activityID.toString(), subjectUsername);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	private Map<Long, Observation.Recording> fetchRecordings(UUID activityID, String subjectUsername) throws SQLException {
		DatabaseConnection conn = server.getConnection();
		CachedRowSet rowSet = conn.executeQuery("select * from recordings where activity = ? and subject = ?", activityID.toString(), subjectUsername);
		
		HashMap<Long, Observation.Recording> map = new HashMap<>();
		
		while (rowSet.next()) {
			try {
				map.put(rowSet.getLong("timestamp"), new Observation.Recording(ObjectSerializer.deserializeImage(rowSet.getBytes("gazeposition"), Point.class), Image.deserializeImage(rowSet.getBytes("screenshot"))));
			} catch (IOException e) {
				// Invalid image format in DB - skip record
				continue;
			}
		}
		
		return map;
	}
}
