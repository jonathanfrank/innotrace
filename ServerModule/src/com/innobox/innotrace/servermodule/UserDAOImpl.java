/**
 * File:		UserDAOImpl.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 17.09.17
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.IUserDAO;
import com.innobox.innotrace.data.User;
import com.innobox.innotrace.data.User.Role;

/**
 * @author jonathanfrank
 * 
 */
public class UserDAOImpl extends UnicastRemoteObject implements IUserDAO {
	
	private transient RMIServer server;
	private transient IDataAccessService dataAccessService;
	
	/**
	 * @param server
	 * @param dataAccessService
	 */
	public UserDAOImpl(RMIServer server, IDataAccessService dataAccessService) throws RemoteException {
		this.server = server;
		this.dataAccessService = dataAccessService;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#getUsers()
	 */
	@Override
	public List<User> getUsers() throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select username, name, role from users");
			
			ArrayList<User> list = new ArrayList<>();
			
			while (rowSet.next()) {
				User u = new User(rowSet.getString("username"), rowSet.getString("name"), null, User.Role.valueOf(rowSet.getString("role")), false);
				list.add(u);
			}
			
			return list;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#getUsersByRole(com.innobox.innotrace.data.User.Role)
	 */
	@Override
	public List<User> getUsersByRole(Role r) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select username, name, role from users where role = ?", r.toString());
			
			ArrayList<User> list = new ArrayList<>();
			
			while (rowSet.next()) {
				User u = new User(rowSet.getString("username"), rowSet.getString("name"), null, User.Role.valueOf(rowSet.getString("role")), false);
				list.add(u);
			}
			
			return list;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#getUser(java.lang.String)
	 */
	@Override
	public User getUser(String username) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select username, name, role from users where username = ?", username);
			
			if (rowSet.next()) {
				User u = new User(rowSet.getString("username"), rowSet.getString("name"), null, User.Role.valueOf(rowSet.getString("role")), false);
				return u;
			} else {
				return null;
			}
			
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#addUser(com.innobox.innotrace.data.User)
	 */
	@Override
	public void addUser(User u) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN)) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at oprette en bruger");
		}
		
		try {
			server.getConnection().executeUpdate("insert into users (username, name, password, role) values (?, ?, ?, ?)", u.getUsername(), u.getName(), u.getPassword(), u.getRole().toString());
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#updateUser(com.innobox.innotrace.data.User)
	 */
	@Override
	public void updateUser(User u) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser().equals(u) || !dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN)) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at opdatere en bruger");
		}
		
		try {
			server.getConnection().executeUpdate("update users set name = ?, password = ? where username = ?", u.getName(), u.getPassword(), u.getUsername());
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#removeUser(java.lang.String)
	 */
	@Override
	public void removeUser(String username) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN)) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at slette en bruger");
		}
		
		try {
			server.getConnection().executeUpdate("delete from users where username = ?", username);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IUserDAO#verifyUser(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public boolean verifyUser(String username, String password) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select username, name, role from users where username = ? and password = ?", username, password);
			
			if (rowSet.next()) {
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
}
