/**
 * File:		RMIServer.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 14.35.44
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLException;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Typed;
import javax.swing.JOptionPane;

import com.innobox.innotrace.common.IApplicationService;
import com.innobox.innotrace.data.IDataAccessServiceFactory;

/**
 * @author jonathanfrank
 * 
 */
@Default
@Typed(IApplicationService.class)
public class RMIServer implements IApplicationService {
	
	private Configuration conf;
	private DatabaseConnection conn;
	private IDataAccessServiceFactory serviceFactory;
	private String rmiURL;
	private Registry registry;
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#init(java.lang.String[])
	 */
	@Override
	public void init(String[] args) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#run()
	 */
	@Override
	public void run() {
		while (true) {
			conf = new ConfigurationDialog(conf).showDialog();
			
			if (conf == null) {
				return;
			}
			
			try {
				rmiURL = String.format("rmi://localhost:%d/%s", conf.getRmiPort(), conf.getRmiPath());
				registry = LocateRegistry.createRegistry(conf.getRmiPort());
				
				conn = new DatabaseConnection(conf);
				serviceFactory = new ServiceFactoryImpl(this);
				Naming.rebind(rmiURL, serviceFactory);
				
				break;
			} catch (MalformedURLException | RemoteException ex) {
				JOptionPane.showMessageDialog(null, String.format("<html><b>Kunne ikke oprette server:</b><br>%s", ex.getMessage()), "Fejlmeddelelse", JOptionPane.ERROR_MESSAGE);
			} catch (SQLException ex) {
				JOptionPane.showMessageDialog(null, String.format("<html><b>Kunne ikke forbinde til databasen:</b><br>%s", ex.getMessage()), "Fejlmeddelelse", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		JOptionPane.showMessageDialog(null, String.format("<html><h1>Serveren er kørende</h1><p>URL: rmi://localhost:%d/%s</p><hr/><p>Tryk på stop for at afslutte</p></html>", conf.getRmiPort(), conf.getRmiPath()), "InnoBox InnoTrace(R)", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * @return the conf
	 */
	public Configuration getConfiguration() {
		return conf;
	}
	
	/**
	 * @return the conn
	 */
	public DatabaseConnection getConnection() {
		return conn;
	}
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#destroy()
	 */
	@Override
	public void destroy() {
		if (registry != null) {
			try {
				registry.unbind(rmiURL);
			} catch (Exception e) {
				// NO-OP
			}
		}
		
		if (conn != null) {
			conn.close();
		}
	}
	
}
