/**
 * File:		ConfigurationDialog.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 14.49.46
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jonathanfrank
 * 
 */
public class ConfigurationDialog extends JDialog {
	
	private Configuration configuration;
	private boolean success;
	private boolean editable;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfDbURL;
	private JTextField tfDbUsername;
	private JTextField tfAdminUsername;
	private JPasswordField pfDbPassword;
	private JPasswordField pfAdminPassword;
	private JTextField tfRMIURL;
	private JSpinner spRMIPort;
	
	/**
	 * Create the dialog.
	 */
	public ConfigurationDialog() {
		this(new Configuration(), true);
	}
	
	/**
	 * Create the dialog.
	 */
	public ConfigurationDialog(Configuration conf) {
		this(conf, true);
	}
	
	/**
	 * Create the dialog.
	 */
	public ConfigurationDialog(Configuration conf, boolean editable) {
		if (conf == null) {
			configuration = new Configuration();
		} else {
			configuration = conf;
		}
		
		this.editable = editable;
		
		setModal(true);
		setTitle("InnoBox InnoTrace(R) - Opsætning af server");
		setBounds(100, 100, 450, 360);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Database", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel, "2, 2, 3, 1, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
			{
				JLabel lblDatabaseUrl = new JLabel("URL:");
				panel.add(lblDatabaseUrl, "2, 2, left, default");
			}
			{
				tfDbURL = new JTextField();
				tfDbURL.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setDbURL(tfDbURL.getText());
					}
				});
				panel.add(tfDbURL, "4, 2, fill, default");
				tfDbURL.setColumns(10);
				tfDbURL.setText(configuration.getDbURL());
				tfDbURL.setEditable(editable);
			}
			{
				JLabel lblBrugernavn = new JLabel("Brugernavn:");
				panel.add(lblBrugernavn, "2, 4, left, default");
			}
			{
				tfDbUsername = new JTextField();
				tfDbUsername.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setDbUsername(tfDbUsername.getText());
					}
				});
				panel.add(tfDbUsername, "4, 4, fill, default");
				tfDbUsername.setColumns(10);
				tfDbUsername.setText(configuration.getDbUsername());
				tfDbUsername.setEditable(editable);
			}
			{
				JLabel lblPassword = new JLabel("Password:");
				panel.add(lblPassword, "2, 6, left, default");
			}
			{
				pfDbPassword = new JPasswordField();
				pfDbPassword.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setDbPassword(new String(pfDbPassword.getPassword()));
					}
				});
				panel.add(pfDbPassword, "4, 6, fill, default");
				pfDbPassword.setText(configuration.getDbPassword());
				pfDbPassword.setEditable(editable);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Superbruger", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel, "2, 4, 3, 1, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
			{
				JLabel lblBrugernavn_1 = new JLabel("Brugernavn:");
				panel.add(lblBrugernavn_1, "2, 2, left, default");
			}
			{
				tfAdminUsername = new JTextField();
				tfAdminUsername.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setAdminUsername(tfAdminUsername.getText());
					}
				});
				panel.add(tfAdminUsername, "4, 2, fill, default");
				tfAdminUsername.setColumns(10);
				tfAdminUsername.setText(configuration.getAdminUsername());
				tfAdminUsername.setEditable(editable);
			}
			{
				JLabel lblPassword_1 = new JLabel("Password:");
				panel.add(lblPassword_1, "2, 4, left, default");
			}
			{
				pfAdminPassword = new JPasswordField();
				pfAdminPassword.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setAdminPassword(new String(pfAdminPassword.getPassword()));
					}
				});
				panel.add(pfAdminPassword, "4, 4, fill, default");
				pfAdminPassword.setText(configuration.getAdminPassword());
				pfAdminPassword.setEditable(editable);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Server", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			contentPanel.add(panel, "2, 6, 3, 1, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
			{
				JLabel lblTcpport = new JLabel("TCP-port:");
				panel.add(lblTcpport, "2, 2, left, default");
			}
			{
				spRMIPort = new JSpinner();
				spRMIPort.setModel(new SpinnerNumberModel(1024, 1, 65535, 1));
				spRMIPort.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						configuration.setRmiPort((Integer) spRMIPort.getValue());
					}
				});
				panel.add(spRMIPort, "4, 2");
				spRMIPort.setValue(configuration.getRmiPort());
				spRMIPort.setEnabled(editable);
			}
			{
				JLabel lblRelativUrl = new JLabel("Relativ URL:");
				panel.add(lblRelativUrl, "2, 4, left, default");
			}
			{
				tfRMIURL = new JTextField();
				tfRMIURL.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						configuration.setRmiPath(tfRMIURL.getText());
					}
				});
				panel.add(tfRMIURL, "4, 4, fill, default");
				tfRMIURL.setColumns(10);
				tfRMIURL.setText(configuration.getRmiPath());
				tfRMIURL.setEditable(editable);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						success = true;
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Annuller");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						success = false;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	/**
	 * @return the configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}
	
	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}
	
	/**
	 * Activates the dialog
	 * 
	 * @return the user input if OK is selected, or NULL if Cancel is selected
	 */
	public Configuration showDialog() {
		setVisible(true);
		if (success) {
			configuration.setDbURL(tfDbURL.getText());
			configuration.setDbUsername(tfDbUsername.getText());
			configuration.setDbPassword(new String(pfDbPassword.getPassword()));
			configuration.setAdminUsername(tfAdminUsername.getText());
			configuration.setAdminPassword(new String(pfAdminPassword.getPassword()));
			configuration.setRmiPort((Integer) spRMIPort.getValue());
			configuration.setRmiPath(tfRMIURL.getText());
			return configuration;
		} else {
			return null;
		}
	}
}
