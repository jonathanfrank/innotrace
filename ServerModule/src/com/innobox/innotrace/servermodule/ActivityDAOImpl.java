/**
 * File:		ActivityDAOImpl.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 17.12.15
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.sql.rowset.CachedRowSet;

import com.innobox.innotrace.data.Activity;
import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IActivityDAO;
import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.PictureActivity;
import com.innobox.innotrace.data.User;
import com.innobox.innotrace.util.Image;

/**
 * @author jonathanfrank
 * 
 */
public class ActivityDAOImpl extends UnicastRemoteObject implements IActivityDAO {
	
	private transient RMIServer server;
	private transient IDataAccessService dataAccessService;
	
	/**
	 * @param server
	 * @param dataAccessService
	 */
	public ActivityDAOImpl(RMIServer server, IDataAccessService dataAccessService) throws RemoteException {
		this.server = server;
		this.dataAccessService = dataAccessService;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivities()
	 */
	@Override
	public List<Activity> getActivities() throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select id, type from activities");
			
			ArrayList<Activity> list = new ArrayList<>();
			
			while (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						list.add(pa);
					}
				} else {
					// Unsupported activity
				}
			}
			
			return list;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivitiesByOwner(java.lang.String)
	 */
	@Override
	public List<Activity> getActivitiesByOwner(String ownerUsername) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select id, type from activities where owner = ?", ownerUsername);
			
			ArrayList<Activity> list = new ArrayList<>();
			
			while (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						list.add(pa);
					}
				} else {
					// Unsupported activity
				}
			}
			
			return list;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivitiesBySubject(java.lang.String)
	 */
	@Override
	public List<Activity> getActivitiesBySubject(String subjectUsername) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select activities.id, activities.type from activities inner join activity_subjects on activities.id = activity_subjects.activity where activity_subjects.subject = ?", subjectUsername);
			
			ArrayList<Activity> list = new ArrayList<>();
			
			while (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						list.add(pa);
					}
				} else {
					// Unsupported activity
				}
			}
			
			return list;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivities(boolean)
	 */
	@Override
	public List<Activity> getActivities(boolean finished) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet;
			
			if (finished) {
				rowSet = conn.executeQuery("select activities.id, activities.type from activities inner join observations on activities.id = observations.activity");
			} else {
				rowSet = conn.executeQuery("select activities.id, activities.type from activities where not exists (select 1 from observations where observations.activity = activities.id)");
			}
			
			ArrayList<Activity> list = new ArrayList<>();
			
			while (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						list.add(pa);
					}
				} else {
					// Unsupported activity
				}
			}
			
			return list;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivitiesBySubject(java.lang.String,
	 *      boolean)
	 */
	@Override
	public List<Activity> getActivitiesBySubject(String subjectUsername, boolean finished) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet;
			
			if (finished) {
				rowSet = conn.executeQuery("select activities.id, activities.type from activities inner join observations on activities.id = observations.activity where observations.subject = ?", subjectUsername);
			} else {
				rowSet = conn.executeQuery("select activities.id, activities.type from activities inner join activity_subjects on activities.id = activity_subjects.activity where activity_subjects.subject = ? and not exists (select 1 from observations where observations.activity = activities.id)", subjectUsername);
			}
			
			ArrayList<Activity> list = new ArrayList<>();
			
			while (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						list.add(pa);
					}
				} else {
					// Unsupported activity
				}
			}
			
			return list;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#getActivity(java.util.UUID)
	 */
	@Override
	public Activity getActivity(UUID activityID) throws DataAccessException, RemoteException {
		try {
			DatabaseConnection conn = server.getConnection();
			CachedRowSet rowSet = conn.executeQuery("select id, type from activities");
			
			if (rowSet.next()) {
				if (rowSet.getString("type").equals("com.innobox.innotrace.data.PictureActivity")) {
					PictureActivity pa = fetchPicitureActivity(UUID.fromString(rowSet.getString("id")));
					if (pa != null) {
						return pa;
					}
				} else {
					// Unsupported activity
				}
			}
			
			return null;
			
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#addActivity(com.innobox.innotrace.data.Activity)
	 */
	@Override
	public void addActivity(Activity a) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !(dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN) || dataAccessService.getCurrentUser().getRole().equals(User.Role.EXPERIMENTER))) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at oprette en aktivitet");
		}
		
		try {
			server.getConnection().executeUpdate("insert into activities (id, type, name, date, instructions, timeout, owner, recordscreen, recordgaze) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", a.getId().toString(), a.getType(), a.getName(), a.getDate(), a.getInstructions(), a.getTimeout(), a.getOwner(), a.isRecordScreen(), a.isRecordGaze());
			
			for (String subject : a.getIntendedSubjects()) {
				server.getConnection().executeUpdate("insert into activity_subjects (activity, subject) values (?, ?)", a.getId().toString(), subject);
			}
			
			if (a instanceof PictureActivity) {
				PictureActivity pa = (PictureActivity) a;
				server.getConnection().executeUpdate("insert into pictureactivities (id, image) values (?, ?)", a.getId().toString(), Image.serializeImage(pa.getPicture()));
			} else {
				// Unsupported activity type
			}
			
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IActivityDAO#removeActivity(java.util.UUID)
	 */
	@Override
	public void removeActivity(UUID activityID) throws DataAccessException, RemoteException {
		if (dataAccessService.getCurrentUser() == null || !(dataAccessService.getCurrentUser().getRole().equals(User.Role.ADMIN) || dataAccessService.getCurrentUser().getRole().equals(User.Role.EXPERIMENTER))) {
			throw new DataAccessException("Du har ikke de fornødne rettigheder til at slette en aktivitet");
		}
		
		try {
			server.getConnection().executeUpdate("delete from activities where activity = ?", activityID.toString());
		} catch (SQLException e) {
			throw new DataAccessException(e);
		}
	}
	
	private PictureActivity fetchPicitureActivity(UUID activityID) throws SQLException {
		DatabaseConnection conn = server.getConnection();
		CachedRowSet act_base = conn.executeQuery("select * from activities where id = ?", activityID.toString());
		CachedRowSet act_subclass = conn.executeQuery("select * from pictureactivities where id = ?", activityID.toString());
		
		if (act_base.next() && act_subclass.next()) {
			try {
				PictureActivity pa = new PictureActivity(UUID.fromString(act_base.getString("id")), act_base.getString("name"), act_base.getDate("date"), act_base.getString("instructions"), act_base.getLong("timeout"), act_base.getString("owner"), null, act_base.getBoolean("recordscreen"), act_base.getBoolean("recordgaze"), Image.deserializeImage(act_subclass.getBytes("image")));
				pa.setIntendedSubjects(fetchIntendedSubjects(activityID));
				return pa;
			} catch (IOException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	private List<String> fetchIntendedSubjects(UUID activityID) throws SQLException {
		DatabaseConnection conn = server.getConnection();
		CachedRowSet act_subjects = conn.executeQuery("select subject from activity_subjects where activity = ?", activityID.toString());
		
		ArrayList<String> list = new ArrayList<>();
		
		while (act_subjects.next()) {
			list.add(act_subjects.getString("subject"));
		}
		
		return list;
	}
}
