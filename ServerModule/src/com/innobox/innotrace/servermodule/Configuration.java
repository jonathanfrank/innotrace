/**
 * File:		Configuration.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 15.04.16
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

/**
 * @author jonathanfrank
 * 
 */
public class Configuration {
	private String dbURL = "jdbc:mysql://localhost/innotrace";
	private String dbUsername = "root";
	private String dbPassword;
	private String adminUsername = "admin";
	private String adminPassword;
	private String rmiPath = "innotrace";
	private int rmiPort = 1099;
	
	/**
	 * 
	 */
	public Configuration() {
		
	}
	
	/**
	 * @param dbURL
	 * @param dbUsername
	 * @param dbPassword
	 * @param adminUsername
	 * @param adminPassword
	 * @param rmiPath
	 * @param rmiPort
	 */
	public Configuration(String dbURL, String dbUsername, String dbPassword, String adminUsername, String adminPassword, String rmiPath, int rmiPort) {
		this.dbURL = dbURL;
		this.dbUsername = dbUsername;
		this.dbPassword = dbPassword;
		this.adminUsername = adminUsername;
		this.adminPassword = adminPassword;
		this.rmiPath = rmiPath;
		this.rmiPort = rmiPort;
	}
	
	/**
	 * @return the dbURL
	 */
	public String getDbURL() {
		return dbURL;
	}
	
	/**
	 * @param dbURL
	 *            the dbURL to set
	 */
	public void setDbURL(String dbURL) {
		this.dbURL = dbURL;
	}
	
	/**
	 * @return the dbUsername
	 */
	public String getDbUsername() {
		return dbUsername;
	}
	
	/**
	 * @param dbUsername
	 *            the dbUsername to set
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	
	/**
	 * @return the dbPassword
	 */
	public String getDbPassword() {
		return dbPassword;
	}
	
	/**
	 * @param dbPassword
	 *            the dbPassword to set
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
	/**
	 * @return the adminUsername
	 */
	public String getAdminUsername() {
		return adminUsername;
	}
	
	/**
	 * @param adminUsername
	 *            the adminUsername to set
	 */
	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}
	
	/**
	 * @return the adminPassword
	 */
	public String getAdminPassword() {
		return adminPassword;
	}
	
	/**
	 * @param adminPassword
	 *            the adminPassword to set
	 */
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	
	/**
	 * @return the rmiPath
	 */
	public String getRmiPath() {
		return rmiPath;
	}
	
	/**
	 * @param rmiPath
	 *            the rmiPath to set
	 */
	public void setRmiPath(String rmiPath) {
		this.rmiPath = rmiPath;
	}
	
	/**
	 * @return the rmiPort
	 */
	public int getRmiPort() {
		return rmiPort;
	}
	
	/**
	 * @param rmiPort
	 *            the rmiPort to set
	 */
	public void setRmiPort(int rmiPort) {
		this.rmiPort = rmiPort;
	}
	
}
