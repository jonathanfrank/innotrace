/**
 * File:		ServiceFactoryImpl.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 16.31.44
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.IDataAccessServiceFactory;

/**
 * @author jonathanfrank
 * 
 */
public class ServiceFactoryImpl extends UnicastRemoteObject implements IDataAccessServiceFactory {
	
	private transient RMIServer server;
	
	/**
	 * @throws RemoteException
	 */
	protected ServiceFactoryImpl(RMIServer server) throws RemoteException {
		this.server = server;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessServiceFactory#createDataAccessService()
	 */
	@Override
	public IDataAccessService createDataAccessService() throws RemoteException {
		return new ServiceImpl(server);
	}
	
}
