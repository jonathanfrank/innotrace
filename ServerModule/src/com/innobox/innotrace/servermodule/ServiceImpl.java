/**
 * File:		ServiceImpl.java
 * Package:		com.innobox.innotrace.servermodule
 * Project:		ServerModule
 * Created:		12/04/2014 16.34.50
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.servermodule;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IActivityDAO;
import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.IObservationDAO;
import com.innobox.innotrace.data.IUserDAO;
import com.innobox.innotrace.data.User;

/**
 * @author jonathanfrank
 * 
 */
public class ServiceImpl extends UnicastRemoteObject implements IDataAccessService {
	
	private transient RMIServer server;
	private transient User currentUser;
	
	/**
	 * @throws RemoteException
	 */
	protected ServiceImpl(RMIServer server) throws RemoteException {
		this.server = server;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#authenticate(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public void authenticate(String username, String password) throws DataAccessException, RemoteException {
		if (username.equals(server.getConfiguration().getAdminUsername()) && password.equals(server.getConfiguration().getAdminPassword())) {
			currentUser = new User(server.getConfiguration().getAdminUsername(), "Administrator", server.getConfiguration().getAdminPassword(), User.Role.ADMIN, true);
			return;
		}
		
		IUserDAO dao = createUserDAO();
		if (dao.verifyUser(username, password)) {
			currentUser = dao.getUser(username);
		} else {
			throw new DataAccessException("Brugernavn og/eller adgangskode er forkert.");
		}
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#deauthenticate()
	 */
	@Override
	public void deauthenticate() throws RemoteException {
		currentUser = null;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#getCurrentUser()
	 */
	@Override
	public User getCurrentUser() throws RemoteException {
		return currentUser;
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#createActivityDAO()
	 */
	@Override
	public IActivityDAO createActivityDAO() throws DataAccessException, RemoteException {
		return new ActivityDAOImpl(server, this);
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#createObservationDAO()
	 */
	@Override
	public IObservationDAO createObservationDAO() throws DataAccessException, RemoteException {
		return new ObservationDAOImpl(server, this);
	}
	
	/**
	 * @see com.innobox.innotrace.data.IDataAccessService#createUserDAO()
	 */
	@Override
	public IUserDAO createUserDAO() throws DataAccessException, RemoteException {
		return new UserDAOImpl(server, this);
	}
	
}
