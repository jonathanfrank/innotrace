/**
 * File:		EntryPoint.java
 * Package:		com.innobox.innotrace.common
 * Project:		BaseModule
 * Created:		20/03/2014 20.08.16
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.common;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * The common entry point for applications based on this module
 * 
 * @author jonathanfrank
 * 
 */
public class EntryPoint implements Runnable {
	
	private IApplicationService app;
	private WeldContainer weldContainer;
	
	private String[] args;
	
	private Throwable runtimeError;
	
	/**
	 * Creates a new EntryPoint
	 * 
	 * @param args
	 *            the command line arguments to the application
	 */
	private EntryPoint(WeldContainer weldContainer, String[] args) {
		this.weldContainer = weldContainer;
		this.args = args;
	}
	
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			app = weldContainer.instance().select(IApplicationService.class).get();
			app.init(args);
			
			Thread appThread = new Thread(app);
			appThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread t, Throwable e) {
					runtimeError = e;
				}
			});
			appThread.start();
			appThread.join();
			
			app.destroy();
		} catch (Throwable t) {
			runtimeError = t;
		} finally {
			if (runtimeError != null) {
				Logger.getLogger(getClass().getSimpleName()).log(Level.SEVERE, "Application exited due to an unrecoverable error", runtimeError);
				JOptionPane.showMessageDialog(null, "Programmet lukkede på grund af en uventet fejl.", "InnoBox InnoTrace(R) Testmodul", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
		}
	}
	
	/**
	 * Runs the application
	 * 
	 * @param args
	 *            the command line arguments to the application
	 */
	public static void main(String[] args) {
		SplashScreen splash = new SplashScreen();
		splash.setVisible(true);
		
		Weld weld = new Weld();
		WeldContainer weldContainer = weld.initialize();
		
		EntryPoint ep = new EntryPoint(weldContainer, args);
		
		splash.setVisible(false);
		
		ep.run();
		
		weld.shutdown();
		
		System.exit(0);
	}
	
}
