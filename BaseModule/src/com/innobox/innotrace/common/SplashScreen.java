/**
 * File:	SplashScreen.java
 * Package:	com.innobox.innotrace.common
 * Project:	BaseModule
 * Author:	jona4946
 * Created:	01/04/2014 14.30.49
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.common;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * A splash screen visible at startup
 * 
 * @author jona4946
 * 
 */
public class SplashScreen {

	private JFrame splashScreen;
	private JLabel splashLabel;

	/**
	 * Creates a new splash screen
	 */
	public SplashScreen() {
		splashScreen = new JFrame();
		splashScreen.setUndecorated(true);
		splashScreen.setSize(320, 240);
		splashScreen.setLocationRelativeTo(null);

		splashScreen.getContentPane().setLayout(new BorderLayout());
		splashScreen.getContentPane().setBackground(new Color(0, 51, 102));
		splashLabel = new JLabel("<html><h1>InnoBox InnoTrace(R)</h1><b><p>Computer-aided UX testing</p><p>&copy; 2014 InnoBox / Jonathan Frank.</p><p>All rights reserved</p><hr/><p>Se den vedlagte licensfil for yderligere detaljer</p></b></html>");
		splashLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		splashLabel.setForeground(Color.white);
		splashScreen.add(splashLabel, BorderLayout.CENTER);
		splashScreen.revalidate();
	}

	/**
	 * Checks whether the splash screen is visible
	 * 
	 * @return true if it is visible
	 */
	public boolean getVisible() {
		return splashScreen.isVisible();
	}

	/**
	 * Displays or closes the splash screen
	 * 
	 * @param visible
	 *            The visibility of the splash screen (true = displayed, false =
	 *            closed)
	 */
	public void setVisible(boolean visible) {
		splashScreen.setVisible(visible);
	}

	/**
	 * Pauses the current thread for a specific period
	 * 
	 * @param time
	 *            the period in seconds
	 */
	public void pause(int time) {
		try {
			Thread.sleep(time * 1000);
		} catch (InterruptedException e) {
			// NO-OP
		}
	}

}
