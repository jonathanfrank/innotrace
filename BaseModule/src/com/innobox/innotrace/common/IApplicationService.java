/**
 * File:		IApplicationService.java
 * Package:		com.innobox.innotrace.common
 * Project:		BaseModule
 * Created:		20/03/2014 20.08.48
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.common;

/**
 * Defines a common interface for runnable applications
 * 
 * @author jonathanfrank
 * 
 */
public interface IApplicationService extends Runnable {
	/**
	 * Initializes the application using an array of command line arguments
	 * 
	 * @param args
	 *            the command line arguments to the application
	 */
	public void init(String[] args);

	/**
	 * Runs the application
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run();

	/**
	 * Terminates the application and releases any resource consumed by the
	 * application
	 */
	public void destroy();
}
