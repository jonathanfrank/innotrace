/**
 * File:		IActivityDAO.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		07/04/2014 18.18.06
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

/**
 * @author jonathanfrank
 * 
 */
public interface IActivityDAO extends Remote {
	public List<Activity> getActivities() throws DataAccessException, RemoteException;
	
	public List<Activity> getActivitiesByOwner(String ownerUsername) throws DataAccessException, RemoteException;
	
	public List<Activity> getActivitiesBySubject(String subjectUsername) throws DataAccessException, RemoteException;
	
	public List<Activity> getActivities(boolean finished) throws DataAccessException, RemoteException;
	
	public List<Activity> getActivitiesBySubject(String subjectUsername, boolean finished) throws DataAccessException, RemoteException;
	
	public Activity getActivity(UUID activityID) throws DataAccessException, RemoteException;
	
	public void addActivity(Activity a) throws DataAccessException, RemoteException;
	
	public void removeActivity(UUID activityID) throws DataAccessException, RemoteException;
}
