/**
 * File:	User.java
 * Package:	com.innobox.innotrace.data
 * Project:	BaseModule
 * Author:	jona4946
 * Created:	31/03/2014 08.22.10
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.data;

import java.io.Serializable;

/**
 * @author jona4946
 * 
 */
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String name;
	private String password;
	private Role role;
	private boolean virtual;
	
	public static enum Role {
		SUBJECT, EXPERIMENTER, ADMIN;
	}
	
	/**
	 * @param username
	 * @param name
	 * @param password
	 * @param role
	 * @param virtual
	 */
	public User(String username, String name, String password, Role role, boolean virtual) {
		this.username = username;
		this.name = name;
		this.password = password;
		this.role = role;
		this.virtual = virtual;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}
	
	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}
	
	/**
	 * @return the virtual
	 */
	public boolean isVirtual() {
		return virtual;
	}
	
	/**
	 * @param virtual
	 *            the virtual to set
	 */
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}
	
}
