/**
 * File:		IUserDAO.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		07/04/2014 18.18.35
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author jonathanfrank
 * 
 */
public interface IUserDAO extends Remote {
	public List<User> getUsers() throws DataAccessException, RemoteException;
	
	public List<User> getUsersByRole(User.Role r) throws DataAccessException, RemoteException;
	
	public boolean verifyUser(String username, String password) throws DataAccessException, RemoteException;
	
	public User getUser(String username) throws DataAccessException, RemoteException;
	
	public void addUser(User u) throws DataAccessException, RemoteException;
	
	public void updateUser(User u) throws DataAccessException, RemoteException;
	
	public void removeUser(String username) throws DataAccessException, RemoteException;
}
