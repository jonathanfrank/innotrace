/**
 * File:	Observation.java
 * Package:	com.innobox.innotrace.data
 * Project:	BaseModule
 * Author:	jona4946
 * Created:	31/03/2014 08.21.56
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.data;

import java.awt.Point;
import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import com.innobox.innotrace.util.Image;

/**
 * @author jona4946
 * 
 */
public class Observation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UUID activityID;
	private String subject;
	private boolean cancellation;
	private Map<Long, Recording> recordings;
	
	public static class Recording implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Point gazePosition;
		private Image screenshot;
		
		/**
		 * @param gazePosition
		 * @param screenshot
		 */
		public Recording(Point gazePosition, Image screenshot) {
			this.gazePosition = gazePosition;
			this.screenshot = screenshot;
		}
		
		/**
		 * @return the gazePosition
		 */
		public Point getGazePosition() {
			return gazePosition;
		}
		
		/**
		 * @param gazePosition
		 *            the gazePosition to set
		 */
		public void setGazePosition(Point gazePosition) {
			this.gazePosition = gazePosition;
		}
		
		/**
		 * @return the screenshot
		 */
		public Image getScreenshot() {
			return screenshot;
		}
		
		/**
		 * @param screenshot
		 *            the screenshot to set
		 */
		public void setScreenshot(Image screenshot) {
			this.screenshot = screenshot;
		}
		
	}
	
	/**
	 * @param activityID
	 * @param subject
	 * @param cancellation
	 * @param recordings
	 */
	public Observation(UUID activityID, String subject, boolean cancellation, Map<Long, Recording> recordings) {
		super();
		this.activityID = activityID;
		this.subject = subject;
		this.cancellation = cancellation;
		this.recordings = recordings;
	}
	
	/**
	 * @return the activityID
	 */
	public UUID getActivityID() {
		return activityID;
	}
	
	/**
	 * @param activityID
	 *            the activityID to set
	 */
	public void setActivityID(UUID activityID) {
		this.activityID = activityID;
	}
	
	/**
	 * @return the cancellation
	 */
	public boolean isCancellation() {
		return cancellation;
	}
	
	/**
	 * @param cancellation
	 *            the cancellation to set
	 */
	public void setCancellation(boolean cancellation) {
		this.cancellation = cancellation;
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * @return the recordings
	 */
	public Map<Long, Recording> getRecordings() {
		return recordings;
	}
	
	/**
	 * @param recordings
	 *            the recordings to set
	 */
	public void setRecordings(Map<Long, Recording> recordings) {
		this.recordings = recordings;
	}
	
}
