/**
 * File:	PictureActivity.java
 * Package:	com.innobox.innotrace.data
 * Project:	BaseModule
 * Author:	jona4946
 * Created:	01/04/2014 15.42.05
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.innobox.innotrace.util.Image;

/**
 * @author jona4946
 * 
 */
public class PictureActivity extends Activity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Image picture;
	
	/**
	 * @param id
	 * @param name
	 * @param date
	 * @param instructions
	 * @param owner
	 * @param intendedSubjects
	 * @param recordScreen
	 * @param recordGaze
	 * @param picture
	 * @param timeout
	 */
	public PictureActivity(UUID id, String name, Date date, String instructions, long timeout, String owner, List<String> intendedSubjects, boolean recordScreen, boolean recordGaze, Image picture) {
		super(id, name, date, instructions, timeout, owner, intendedSubjects, recordScreen, recordGaze);
		this.picture = picture;
	}
	
	/**
	 * @see com.innobox.innotrace.data.Activity#getType()
	 */
	@Override
	public String getType() {
		return "com.innobox.innotrace.data.PictureActivity";
	}
	
	/**
	 * @return the picture
	 */
	public Image getPicture() {
		return picture;
	}
	
	/**
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(Image picture) {
		this.picture = picture;
	}
	
}
