/**
 * File:		IDataAccessService.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		07/04/2014 18.17.43
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author jonathanfrank
 * 
 */
public interface IDataAccessService extends Remote {
	public void authenticate(String username, String password) throws DataAccessException, RemoteException;
	
	public void deauthenticate() throws RemoteException;
	
	public User getCurrentUser() throws RemoteException;
	
	public IActivityDAO createActivityDAO() throws DataAccessException, RemoteException;
	
	public IObservationDAO createObservationDAO() throws DataAccessException, RemoteException;
	
	public IUserDAO createUserDAO() throws DataAccessException, RemoteException;
}
