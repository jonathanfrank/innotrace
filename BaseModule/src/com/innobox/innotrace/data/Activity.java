/**
 * File:	Activity.java
 * Package:	com.innobox.innotrace.data
 * Project:	BaseModule
 * Author:	jona4946
 * Created:	31/03/2014 08.21.35
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author jona4946
 * 
 */
public abstract class Activity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UUID id;
	private String name;
	private Date date;
	private String instructions;
	private long timeout;
	private String owner;
	private List<String> intendedSubjects;
	private boolean recordScreen;
	private boolean recordGaze;
	
	/**
	 * @param id
	 * @param name
	 * @param date
	 * @param instructions
	 * @param owner
	 * @param intendedSubjects
	 * @param recordScreen
	 * @param recordGaze
	 */
	public Activity(UUID id, String name, Date date, String instructions, long timeout, String owner, List<String> intendedSubjects, boolean recordScreen, boolean recordGaze) {
		this.id = id;
		this.name = name;
		this.date = date;
		this.instructions = instructions;
		this.timeout = timeout;
		this.owner = owner;
		this.intendedSubjects = intendedSubjects;
		this.recordScreen = recordScreen;
		this.recordGaze = recordGaze;
	}
	
	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}
	
	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * @return the instructions
	 */
	public String getInstructions() {
		return instructions;
	}
	
	/**
	 * @param instructions
	 *            the instructions to set
	 */
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	
	/**
	 * @param owner
	 *            the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	/**
	 * @return the intendedSubjects
	 */
	public List<String> getIntendedSubjects() {
		return intendedSubjects;
	}
	
	/**
	 * @param intendedSubjects
	 *            the intendedSubjects to set
	 */
	public void setIntendedSubjects(List<String> intendedSubjects) {
		this.intendedSubjects = intendedSubjects;
	}
	
	/**
	 * @return the recordScreen
	 */
	public boolean isRecordScreen() {
		return recordScreen;
	}
	
	/**
	 * @param recordScreen
	 *            the recordScreen to set
	 */
	public void setRecordScreen(boolean recordScreen) {
		this.recordScreen = recordScreen;
	}
	
	/**
	 * @return the recordGaze
	 */
	public boolean isRecordGaze() {
		return recordGaze;
	}
	
	/**
	 * @param recordGaze
	 *            the recordGaze to set
	 */
	public void setRecordGaze(boolean recordGaze) {
		this.recordGaze = recordGaze;
	}
	
	public abstract String getType();
	
	/**
	 * @return the timeout
	 */
	public long getTimeout() {
		return timeout;
	}
	
	/**
	 * @param timeout
	 *            the timeout to set
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
}
