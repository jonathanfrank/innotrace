/**
 * File:		IDataAccessServiceFactory.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		12/04/2014 12.09.13
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author jonathanfrank
 * 
 */
public interface IDataAccessServiceFactory extends Remote {
	public IDataAccessService createDataAccessService() throws RemoteException;
}
