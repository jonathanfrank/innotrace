/**
 * File:		DataAccessException.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		07/04/2014 18.19.19
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

/**
 * @author jonathanfrank
 * 
 */
public class DataAccessException extends Exception {
	
	/**
	 * 
	 */
	public DataAccessException() {
		
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public DataAccessException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * @param message
	 */
	public DataAccessException(String message) {
		super(message);
	}
	
	/**
	 * @param cause
	 */
	public DataAccessException(Throwable cause) {
		super(cause);
	}
	
}
