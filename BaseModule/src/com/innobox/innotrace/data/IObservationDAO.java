/**
 * File:		IObservationDAO.java
 * Package:		com.innobox.innotrace.data
 * Project:		BaseModule
 * Created:		07/04/2014 18.18.19
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.data;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

/**
 * @author jonathanfrank
 * 
 */
public interface IObservationDAO extends Remote {
	public List<Observation> getObservationsByActivity(UUID activityID) throws DataAccessException, RemoteException;
	
	public Observation getObservation(UUID activityID, String subjectUsername) throws DataAccessException, RemoteException;
	
	public void addObservation(Observation o) throws DataAccessException, RemoteException;
	
	public void removeObservation(UUID activityID, String subjectUsername) throws DataAccessException, RemoteException;
}
