/**
 * File:		Image.java
 * Package:		com.innobox.innotrace.util
 * Project:		BaseModule
 * Created:		03/04/2014 19.29.25
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;

/**
 * A compressed serializable wrapper around BufferedImage
 * 
 * @author jonathanfrank
 * 
 */
public class Image implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * The only available encoding
	 */
	private static final String DEFAULT_ENCODING = "png";
	
	/**
	 * The encoded image
	 */
	private byte[] data;
	/**
	 * A decompressed image cache
	 */
	private transient BufferedImage imgCache;
	
	/**
	 * Creates a new image
	 * 
	 * @param img
	 *            the raw BufferedImage
	 */
	public Image(BufferedImage img) {
		this(img, DEFAULT_ENCODING);
	}
	
	/**
	 * Creates a new image using a non-standard encoding
	 * 
	 * @param img
	 *            the raw BufferedImage
	 * @param encoding
	 *            the encoding
	 */
	public Image(BufferedImage img, String encoding) {
		imgCache = img;
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			ImageIO.write(img, encoding, out);
		} catch (IOException e) {
			throw new InternalError("A ByteArrayOutputStream does not throw IOExceptions");
		}
		data = out.toByteArray();
	}
	
	/**
	 * Returns the raw pixel data as a BufferedImage. The output of this method
	 * is cached to enhance performance.
	 * 
	 * @return the raw pixel data as a BufferedImage
	 */
	public BufferedImage getBufferedImage() {
		if (imgCache != null) {
			return imgCache;
		}
		
		try {
			imgCache = ImageIO.read(new ByteArrayInputStream(data));
		} catch (IOException e) {
			throw new InternalError("A ByteArrayInputStream does not throw IOExceptions");
		}
		return imgCache;
	}
	
	public static byte[] serializeImage(Image image) throws IOException {
		if (image == null) {
			return null;
		}
		
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream(bout);
		oout.writeObject(image);
		oout.flush();
		oout.close();
		return bout.toByteArray();
	}
	
	public static Image deserializeImage(byte[] image) throws IOException {
		if (image == null || image.length == 0) {
			return null;
		}
		
		try {
			ByteArrayInputStream bin = new ByteArrayInputStream(image);
			ObjectInputStream oin = new ObjectInputStream(bin);
			Image i = (Image) oin.readObject();
			oin.close();
			return i;
		} catch (ClassNotFoundException | ClassCastException e) {
			throw new IOException("Invalid image format", e);
		}
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.write(data);
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		
		while (true) {
			try {
				bout.write(in.readByte());
			} catch (EOFException e) {
				break;
			}
		}
		
		data = bout.toByteArray();
	}
}
