/**
 * File:		ImageViewer.java
 * Package:		com.innobox.innotrace.util
 * Project:		BaseModule
 * Created:		12/04/2014 14.00.21
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.util;

import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * @author jonathanfrank
 * 
 */
public class ImageViewer extends JPanel {
	private Image image;
	
	/**
	 * @param image
	 */
	public ImageViewer(Image image) {
		this.image = image;
		
		setOpaque(false);
	}
	
	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}
	
	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(Image image) {
		this.image = image;
		repaint();
	}
	
	/**
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (image == null) {
			return;
		} else {
			g.drawImage(image.getBufferedImage(), 0, 0, getWidth(), getHeight(), null);
		}
	}
	
}
