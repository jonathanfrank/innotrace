/**
 * File:		ObjectSerializer.java
 * Package:		com.innobox.innotrace.util
 * Project:		BaseModule
 * Created:		13/04/2014 10.03.48
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author jonathanfrank
 * 
 */
public class ObjectSerializer {
	public static <T extends Serializable> byte[] serializeObject(T obj) throws IOException {
		if (obj == null) {
			return null;
		}
		
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream(bout);
		oout.writeObject(obj);
		oout.flush();
		oout.close();
		return bout.toByteArray();
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T deserializeImage(byte[] image, Class<T> type) throws IOException {
		if (image == null || image.length == 0) {
			return null;
		}
		
		try {
			ByteArrayInputStream bin = new ByteArrayInputStream(image);
			ObjectInputStream oin = new ObjectInputStream(bin);
			T obj = (T) oin.readObject();
			oin.close();
			return obj;
		} catch (ClassNotFoundException | ClassCastException e) {
			throw new IOException("Invalid image", e);
		}
	}
	
}
