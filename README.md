Udvikling af et værktøj til automatiserede objektive brugertests
================================================================
Jonathan Frank
6. marts 2014

Opgaveformulering
-----------------
Elektronisk gaze tracking er en ny teknologi, der kan anvendes til objektiv undersøgelse af brugeren eller seerens opmærksomhed gennem beregning af øjnenes fokuspunkt, hvilket blandt andet er anvendeligt inden for evaluering af brugergrænseﬂader, hjemmesider, reklamemateriale eller lignende. For at gøre dette kræves et program, der instruerer forsøgspersonen i at udføre en bestemt aktivitet på sin computer, imens øjnenes fokuspunkt samt skærmbilledet optages og lagres i en database til efterfølgende evaluering. Målet med dette projekt er derfor at udvikle et it-system til dette formål bestående af tre komponenter, der hver for sig udfører følgende opgaver:

- En serverkomponent opsætter aktivitet samt forsøgspersoner og optager hver forsøgspersons skærmbillede og fokuspunkt under udførelse af en aktivitet
- En administratorkomponent, der tillader forsøgslederen at opsætte aktiviteter samt at evaluere disse optagelser
- En testkomponent, der tillader en forsøg person at udføre en på forhånd deﬁneret aktivitet, imens skærmbillede og fokuspunkt optages og sendes til serveren

Mulighederne for aktiviteter skal som udgangspunkt omfatte følgende:

- Navigation på en hjemmeside
- Navigation i et computerprogram med en graﬁsk brugergrænseﬂade
- Visning af et stillbillede (fx plakat, foto, brevpapir eller lignende)
- Visning af en video

Evalueringsmetoderne omfatter som udgangspunkt følgende muligheder:

- Samtidig afspilning af en eller ﬂere forsøgspersoners skærmbilleder med deres fokuspunkter lagt oven på billedet
- Et heat map pr. forsøgsperson eller gennesnitligt for hele forsøget, der viser opmærksomheden (O = tFokus/tForsøg) på dele af stillbilleder og videoframes

Der er derudover hensigten, at systemet omfatter følgende brugertyper med hver deres rolle:

- Flere forsøgspersoner, der udfører aktiviteter under testkomponentens observation
- En forsøgsleder, der opsætter og evaluerer aktiviteter gennem administratorkomponenten

Hardwaren (teknologien), som anvendes til elektronisk gaze tracking, inkluderer følgende:

- Dedikeret infrarød gaze tracker af mærket The Eye Tribe (i begyndelsen)
- Almindelige indbyggede webcams (videreudvikling)


Projektplan
-----------
Projektets arbejde koordineres ved hjælp af den inkrementerende iterative arbejdsmetode, hvilket medfører, at arbejdet opdeles i mindre iterationer (faser), hvor enkelte af de førnævnte funktionaliteter implementeres, så andelen af færdigjorte funktionaliteter inkrementerer (øges) over tid. Målet er, at hver iteration indeholder et selvstændigt forløb bestående af analyse, design og test, således at der efter hver iteration er et færdigt program, som er anvendeligt i praksis. Følgende iterationsplan er overvejet:

	Iteration		Komponent			Funktionalitet
	1				Test				Udvikling af et værktøj, der kan udføre en hard-coded
										aktivitet, imens skærmbilledet samt brugerens fokus
										optages og lagres lokalt i en ﬁl. Aktiviteterne stillbillede
										samt video er understøttet i løbet af denne iteration.

	2				Admin				Udvikling af et værktøj, der kan evaluere optagelser
										gemt lokalt i en ﬁl fra værktøjet udviklet under forrige
										iteration. Alle de planlagte evalueringsmetoder
										understøttes i løbet af denne iteration.

	3				Test, admin			Forbedring af test- og adminkomponenterne, så en
										aktivitet kan deﬁneres af forsøgslederen vha.
										adminkomponenten og efterfølgende åbnes af
										forsøgspersonen vha. testkomponenten.

	4				Server				Der udvikles en serverkomponenten, der lagrer
										aktivitetsdeﬁnitioner og forsøgsresultater i en
										SQL-database. Brugerdeﬁnition og adgangskontrol
										indføres på serveren til rollebaserede tilladelser samt
										navngivning af forsøgsresultater.

	5				Test, admin			Test- og adminkomponenterne modiﬁceres, så de
										anvender serverkomponenten som datakilde fremfor det
										lokale ﬁlsystem. Login implementeres i disse
										komponenter af hensyn til adgangskontrol på serveren.

	6, 7, 8 ... n	Test, admin			Yderligere aktivitetstyper og evalueringsmetoder samt
										muligheden for eksport af data til almindelige
										ﬁlformater implementeres.

Det forventes, at det i løbet af projektforløbet er muligt at gennemføre til og med iteration 3, mens de resterende iterationer vil blive dokumenteret på et overordnet plan med henblik på efterfølgende udvidelser af programmet.
