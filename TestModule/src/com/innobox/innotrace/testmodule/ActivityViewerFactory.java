/**
 * File:		ActivityViewerFactory.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		12/04/2014 13.57.31
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.Component;

import com.innobox.innotrace.data.Activity;
import com.innobox.innotrace.data.PictureActivity;
import com.innobox.innotrace.util.ImageViewer;

/**
 * @author jonathanfrank
 * 
 */
public class ActivityViewerFactory {
	
	public Component createActivityViewer(Activity a) {
		if (a instanceof PictureActivity) {
			PictureActivity pa = (PictureActivity) a;
			return new ImageViewer(pa.getPicture());
		}
		
		return null;
	}
}
