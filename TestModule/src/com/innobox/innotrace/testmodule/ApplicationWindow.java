/**
 * File:		ApplicationWindow.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		07/04/2014 20.25.44
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * @author jona4946
 * 
 */
public class ApplicationWindow extends JFrame {
	
	private JPanel contentPane;
	
	private JLabel lblStatusMessage;
	
	private JPanel contents;
	
	private ApplicationController appController;
	
	/**
	 * Create the frame.
	 */
	public ApplicationWindow(ApplicationController appController) {
		this.appController = appController;
		
		setTitle("InnoBox InnoTrace(R) Testmodul");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			
			/**
			 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
			 */
			@Override
			public void windowClosing(WindowEvent arg0) {
				ApplicationWindow.this.appController.requestShutdown();
			}
			
		});
		
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 51, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel toolbar = new JPanel();
		toolbar.setBackground(new Color(0, 51, 102));
		FlowLayout fl_toolbar = (FlowLayout) toolbar.getLayout();
		fl_toolbar.setAlignment(FlowLayout.RIGHT);
		contentPane.add(toolbar, BorderLayout.NORTH);
		
		// TODO Fjern pladsholdertekst før endeligt release
		lblStatusMessage = new JLabel("[statusmeddelelse]");
		lblStatusMessage.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStatusMessage.setForeground(new Color(255, 255, 255));
		toolbar.add(lblStatusMessage);
		
		JButton btnInfo = new JButton("<html><u>Vigtig information");
		btnInfo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ApplicationWindow.this.appController.showAboutDialog();
			}
		});
		btnInfo.setBorderPainted(false);
		btnInfo.setOpaque(false);
		btnInfo.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInfo.setForeground(new Color(255, 255, 255));
		btnInfo.setBackground(new Color(0, 51, 102));
		toolbar.add(btnInfo);
		
		JButton btnLogout = new JButton("<html><u>Log ud");
		btnLogout.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ApplicationWindow.this.appController.logout();
			}
		});
		btnLogout.setBorderPainted(false);
		btnLogout.setOpaque(false);
		btnLogout.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnLogout.setForeground(new Color(255, 255, 255));
		btnLogout.setBackground(new Color(0, 51, 102));
		toolbar.add(btnLogout);
		
		contents = new JPanel();
		contents.setBackground(new Color(255, 255, 255));
		contentPane.add(contents, BorderLayout.CENTER);
		contents.setLayout(new CardLayout(0, 0));
	}
	
	public void setContents(Component component) {
		contents.removeAll();
		contents.add(component);
		revalidate();
	}
	
	public Component getContents() {
		if (contents.getComponentCount() < 1) {
			return null;
		}
		
		return contents.getComponent(0);
	}
	
	public void setStatusMessage(String statusMessage) {
		lblStatusMessage.setText(statusMessage);
	}
	
	public String getStatusMessage() {
		return lblStatusMessage.getText();
	}
}
