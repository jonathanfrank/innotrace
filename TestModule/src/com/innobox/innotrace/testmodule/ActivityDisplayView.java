/**
 * File:		ActivityDisplayView.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		12/04/2014 13.12.58
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.AWTException;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

import com.innobox.innotrace.data.Activity;
import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IObservationDAO;
import com.innobox.innotrace.data.Observation;
import com.innobox.innotrace.data.Observation.Recording;
import com.innobox.innotrace.gazetracking.IGazeTrackingService;
import com.innobox.innotrace.gazetracking.theeyetribe.GazeTrackingServiceImpl;
import com.innobox.innotrace.screenrecording.IScreenRecordingService;
import com.innobox.innotrace.screenrecording.awtrobot.ScreenRecordingServiceImpl;
import com.innobox.innotrace.util.Image;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jonathanfrank
 * 
 */
public class ActivityDisplayView extends JPanel {
	
	private ApplicationController appController;
	
	@Inject
	IGazeTrackingService gazeTrackingService;
	
	@Inject
	IScreenRecordingService screenRecordingService;
	
	private Activity activity;
	private Map<Long, Observation.Recording> recordings;
	private long startTimestamp;
	private Timer recTimer;
	private Timer stopTimer;
	
	/**
	 * Create the panel.
	 */
	public ActivityDisplayView(ApplicationController appController, Activity activity) {
		this.appController = appController;
		this.activity = activity;
		recordings = new HashMap<Long, Observation.Recording>();
		
		// TODO Find ud af, hvorfor CDI ikke virker
		// ----Midlertidigt bugfix---------
		gazeTrackingService = new GazeTrackingServiceImpl();
		try {
			screenRecordingService = new ScreenRecordingServiceImpl();
		} catch (AWTException e1) {
			// NO-OP
		}
		// ----Midlertidigt bugfix slut----
		
		setBackground(Color.GRAY);
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		
		JPanel contents = new JPanel();
		contents.setBackground(Color.GRAY);
		add(contents, "2, 2, fill, fill");
		contents.setLayout(new CardLayout(0, 0));
		
		ActivityViewerFactory avf = new ActivityViewerFactory();
		Component viewer = avf.createActivityViewer(activity);
		
		if (viewer == null) {
			fail("Aktivitetstypen understøttes ikke i denne version af programmet");
			return;
		} else {
			contents.add(viewer);
		}
		
		JButton btnFinish = new JButton("Afslut");
		btnFinish.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stop();
			}
		});
		add(btnFinish, "2, 4, right, default");
		
		if (activity.getTimeout() > 0) {
			btnFinish.setEnabled(false);
		}
		
	}
	
	protected void start() {
		if (activity.isRecordGaze()) {
			if (gazeTrackingService == null || !gazeTrackingService.isAvailable()) {
				fail("Der er ikke nogen fungerende gaze tracker til stede.");
				return;
			}
			
			if (!gazeTrackingService.isCalibrated()) {
				appController.showInfoDialog("<html>Gaze trackeren skal kalibreres<br>før den kan tages i brug.<br>Dette tager omkring 1 minut.");
				gazeTrackingService.calibrate();
			}
			
			gazeTrackingService.start();
		}
		
		if (activity.isRecordScreen()) {
			if (screenRecordingService == null) {
				fail("Der er ikke nogen fungerende screen recorder til stede.");
				return;
			}
		}
		
		startTimestamp = System.currentTimeMillis();
		
		recTimer = new Timer((int) (1000.0 / 25.0), new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				capture();
			}
		});
		recTimer.setRepeats(true);
		recTimer.start();
		
		if (activity.getTimeout() > 0) {
			stopTimer = new Timer((int) activity.getTimeout() * 1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					stop();
				}
			});
			stopTimer.setRepeats(false);
			stopTimer.start();
		}
		
	}
	
	protected void capture() {
		Point gazePosition = null;
		Image screenshot = null;
		
		if (activity.isRecordGaze()) {
			Point2D p = gazeTrackingService.getLastPosition();
			gazePosition = new Point((int) p.getX(), (int) p.getY());
		}
		
		if (activity.isRecordScreen()) {
			screenshot = new Image(screenRecordingService.getScreenshot());
		}
		
		Observation.Recording rec = new Recording(gazePosition, screenshot);
		recordings.put(System.currentTimeMillis() - startTimestamp, rec);
	}
	
	protected void stop() {
		recTimer.stop();
		
		if (gazeTrackingService.isRunning()) {
			gazeTrackingService.stop();
		}
		
		try {
			IObservationDAO dao = appController.getDataAccessService().createObservationDAO();
			String currentUsername = appController.getDataAccessService().getCurrentUser().getUsername();
			Observation o = new Observation(activity.getId(), currentUsername, false, recordings);
			dao.addObservation(o);
			appController.activityFinished(o);
		} catch (DataAccessException ex) {
			appController.showErrorDialog("<html><b>Serveren afviste forspørgelsen:</b><br>%s", ex.getMessage());
			appController.activityFinished(null);
		} catch (RemoteException ex) {
			appController.showErrorDialog("<html><b>Kunne ikke kommunikere med serveren:</b><br>%s", ex.getMessage());
			appController.activityFinished(null);
		}
	}
	
	protected void fail(String reason) {
		if (recTimer != null && recTimer.isRunning()) {
			recTimer.stop();
		}
		
		if (stopTimer != null && stopTimer.isRunning()) {
			stopTimer.stop();
		}
		
		appController.showErrorDialog("<html><b>Programmet blev forhindret i at optage forsøget:</b><br>%s", reason);
		appController.activitySelection();
	}
}
