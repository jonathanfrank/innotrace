/**
 * File:		LoginView.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		07/04/2014 19.35.59
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jonathanfrank
 * 
 */
public class LoginView extends JPanel {
	private ApplicationController appController;
	private JTextField tfURL;
	private JTextField tfUsername;
	private JPasswordField tfPassword;
	
	/**
	 * Create the panel.
	 */
	public LoginView(ApplicationController appController) {
		setBackground(new Color(255, 255, 255));
		this.appController = appController;
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
		
		JLabel lblProductName = new JLabel("<html><h1>InnoBox InnoTrace(R)</h1><b><p>Computer-aided UX testing</p><p>&copy; 2014 InnoBox / Jonathan Frank.</p><p>All rights reserved</p><hr/><p>Se den vedlagte licensfil for yderligere detaljer</p></b></html>");
		lblProductName.setForeground(new Color(0, 51, 102));
		lblProductName.setFont(new Font("Dialog", Font.BOLD, 12));
		add(lblProductName, "2, 2, 5, 1, left, top");
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Login", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBackground(new Color(255, 255, 255));
		add(panel, "4, 4, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.PARAGRAPH_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.PARAGRAPH_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		
		JLabel lblURL = new JLabel("URL:");
		panel.add(lblURL, "2, 2, right, default");
		
		tfURL = new JTextField();
		tfURL.setText("rmi://localhost/innotrace");
		panel.add(tfURL, "4, 2, 3, 1, fill, default");
		tfURL.setColumns(10);
		
		JLabel lblUsername = new JLabel("Brugernavn:");
		panel.add(lblUsername, "2, 4, right, default");
		
		tfUsername = new JTextField();
		panel.add(tfUsername, "4, 4, 3, 1, fill, default");
		tfUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Adgangskode:");
		panel.add(lblPassword, "2, 6, right, default");
		
		tfPassword = new JPasswordField();
		panel.add(tfPassword, "4, 6, 3, 1, fill, default");
		tfPassword.setColumns(10);
		
		JButton btnExit = new JButton("Afslut");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginView.this.appController.requestShutdown();
			}
		});
		panel.add(btnExit, "4, 8");
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginView.this.appController.login(tfURL.getText(), tfUsername.getText(), new String(tfPassword.getPassword()));
			}
		});
		panel.add(btnLogin, "6, 8");
	}
	
}
