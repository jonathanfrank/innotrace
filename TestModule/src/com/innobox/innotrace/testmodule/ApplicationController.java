/**
 * File:	ApplicationController.java
 * Package:	com.innobox.innotrace.testmodule
 * Project:	TestModule
 * Author:	jona4946
 * Created:	31/03/2014 08.24.38
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.testmodule;

import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Typed;
import javax.swing.JOptionPane;

import com.innobox.innotrace.common.IApplicationService;
import com.innobox.innotrace.data.Activity;
import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IDataAccessService;
import com.innobox.innotrace.data.IDataAccessServiceFactory;
import com.innobox.innotrace.data.Observation;

/**
 * @author jona4946
 * 
 */
@Default
@Typed(IApplicationService.class)
public class ApplicationController implements IApplicationService {
	
	private IDataAccessService dataAccessService;
	private Activity currentActivity;
	
	private Object mutex = new Object();
	private boolean keepRunning;
	
	private ApplicationWindow appWindow;
	private LoginView loginView;
	private ActivityListView activityList;
	private ActivityInstructionsView activityInstructionsView;
	private ActivityDisplayView activityDisplayView;
	private ActivityFinishedView activityFinishedView;
	
	/**
	 * 
	 */
	public ApplicationController() {
		appWindow = new ApplicationWindow(this);
	}
	
	public void activitySelection() {
		activityList = new ActivityListView(this);
		appWindow.setContents(activityList);
	}
	
	public void activitySelected(Activity a) {
		currentActivity = a;
		activityInstructionsView = new ActivityInstructionsView(this, currentActivity);
		appWindow.setContents(activityInstructionsView);
	}
	
	public void activityStarted() {
		activityDisplayView = new ActivityDisplayView(this, currentActivity);
		appWindow.setContents(activityDisplayView);
		activityDisplayView.start();
	}
	
	public void activityFinished(Observation o) {
		activityFinishedView = new ActivityFinishedView(this);
		appWindow.setContents(activityFinishedView);
	}
	
	public void login(String url, String username, String password) {
		try {
			IDataAccessServiceFactory factory = (IDataAccessServiceFactory) Naming.lookup(url);
			dataAccessService = factory.createDataAccessService();
			dataAccessService.authenticate(username, password);
			activitySelection();
			
			appWindow.setStatusMessage(String.format("Velkommen %s", dataAccessService.getCurrentUser().getName()));
			
		} catch (DataAccessException dex) {
			showErrorDialog("<html><b>Serveren accepterede ikke brugernavn og/eller password</b><br>%s", dex.getMessage());
		} catch (Exception ex) {
			showErrorDialog("<html><b>Kunne ikke forbinde til serveren</b><br>%s", ex.getMessage());
		}
	}
	
	public void logout() {
		if (dataAccessService == null) {
			return;
		}
		
		try {
			dataAccessService.deauthenticate();
		} catch (RemoteException ex) {
			showErrorDialog("<html><b>Kunne ikke kommunikere med serveren</b><br>%s", ex.getMessage());
			return;
		}
		
		loginView = new LoginView(this);
		appWindow.setContents(loginView);
		appWindow.setStatusMessage("Velkommen");
	}
	
	public void requestShutdown() {
		synchronized (mutex) {
			keepRunning = false;
			mutex.notifyAll();
		}
	}
	
	public void showAboutDialog() {
		JOptionPane.showMessageDialog(appWindow, "<html><h1>InnoBox InnoTrace(R)</h1><p>Testmodul</p><p>&copy; 2014 InnoBox / Jonathan Frank.</p><p>All rights reserved</p><hr/><p>Se den vedlagte licensfil for yderligere detaljer</p></html>", "Om programmet", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void showInfoDialog(String message, Object... params) {
		JOptionPane.showMessageDialog(appWindow, String.format(message, params), "Information", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void showErrorDialog(String message, Object... params) {
		JOptionPane.showMessageDialog(appWindow, String.format(message, params), "Fejlmeddelelse", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * @return the dataAccessService
	 */
	public IDataAccessService getDataAccessService() {
		return dataAccessService;
	}
	
	/**
	 * @return the currentActivity
	 */
	public Activity getCurrentActivity() {
		return currentActivity;
	}
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#init(java.lang.String[])
	 */
	@Override
	public void init(String[] args) {
		keepRunning = true;
	}
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#run()
	 */
	@Override
	public void run() {
		appWindow.setVisible(true);
		appWindow.setStatusMessage("Velkommen");
		
		loginView = new LoginView(this);
		appWindow.setContents(loginView);
		
		while (true) {
			synchronized (mutex) {
				if (!keepRunning) {
					break;
				}
				
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					// NO-OP
				}
			}
		}
		
	}
	
	/**
	 * @see com.innobox.innotrace.common.IApplicationService#destroy()
	 */
	@Override
	public void destroy() {
		appWindow.setVisible(false);
		
		try {
			dataAccessService.deauthenticate();
		} catch (Exception e) {
			// NO-OP
		}
	}
	
}
