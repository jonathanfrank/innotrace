/**
 * File:	ActivityTableModel.java
 * Package:	com.innobox.innotrace.testmodule
 * Project:	TestModule
 * Author:	jona4946
 * Created:	01/04/2014 14.06.15
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.testmodule;

import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.innobox.innotrace.data.Activity;

/**
 * @author jona4946
 * 
 */
public class ActivityTableModel extends AbstractTableModel {
	
	private List<Activity> activities;
	
	private static final String[] headers = { "Navn", "Type", "Dato" };
	
	/**
	 * 
	 */
	public ActivityTableModel() {
		
	}
	
	/**
	 * @param activities
	 */
	public ActivityTableModel(List<Activity> activities) {
		this.activities = activities;
		fireTableDataChanged();
	}
	
	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return headers.length;
	}
	
	/**
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		if (activities == null)
			return 0;
		
		return activities.size();
	}
	
	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int col) {
		Activity act = activities.get(row);
		
		if (act == null) {
			return null;
		}
		
		switch (col) {
		case 0:
			return act.getName();
		case 1:
			return act.getType();
		case 2:
			return act.getDate();
		default:
			return null;
		}
	}
	
	public Activity getRowAt(int row) {
		return activities.get(row);
	}
	
	/**
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int col) {
		switch (col) {
		case 0:
			return String.class;
		case 1:
			return String.class;
		case 2:
			return Date.class;
		default:
			return null;
		}
	}
	
	/**
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int col) {
		return headers[col];
	}
	
	/**
	 * @return the activities
	 */
	public List<Activity> getActivities() {
		return activities;
	}
	
	/**
	 * @param activities
	 *            the activities to set
	 */
	public void setActivities(List<Activity> activities) {
		this.activities = activities;
		fireTableDataChanged();
	}
	
}
