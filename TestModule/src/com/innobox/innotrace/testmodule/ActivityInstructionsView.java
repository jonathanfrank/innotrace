/**
 * File:		ActivityInstructionsView.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		07/04/2014 18.46.39
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.innobox.innotrace.data.Activity;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jonathanfrank
 * 
 */
public class ActivityInstructionsView extends JPanel {
	
	private ApplicationController appController;
	private Activity activity;
	
	/**
	 * Create the panel.
	 */
	public ActivityInstructionsView(ApplicationController appController, Activity activity) {
		this.appController = appController;
		this.activity = activity;
		
		setBackground(Color.WHITE);
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("min(520px;default):grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		
		JLabel lblHeader = new JLabel("Læs følgende instruktioner grundigt");
		lblHeader.setFont(new Font("Dialog", Font.BOLD, 24));
		lblHeader.setForeground(new Color(0, 51, 102));
		add(lblHeader, "2, 2");
		
		JLabel lblInstructions = new JLabel("<html>\n<p>\nFormålet med denne aktivitet er ved hjælp af en sensor placeret foran computeren at undersøge, hvor dine øjne fokuserer, imens du betragter den fremviste mediefil. Det er derfor vigtigt, at du sikrer dig følgende, så forsøgsresultatet kan blive så nøjagtigt som muligt:\n</p>\n<ul>\n<li>Placer dig med 50 cm afstand fra computerskærmen\n<li>Juster stolens højde og placering, så du ser direkte ind i skærmen\n<li>Forsøg at være så afslappet som muligt\n<li>Betragt den fremviste mediefil kontinuerligt, indtil aktiviteten automatisk afbrydes efter et på forhånd defineret tidsinterval\n</ul>\n</html>");
		lblInstructions.setFont(new Font("Verdana", Font.PLAIN, 14));
		add(lblInstructions, "2, 4, left, top");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(new Color(255, 255, 255));
		add(scrollPane, "2, 6, fill, fill");
		
		// TODO Fjern pladsholdertekst før endelig udgivelse af programmet
		JLabel lblCustomInstructions = new JLabel("[brugerdefinerede instruktioner]");
		
		if (this.activity.getInstructions() != null) {
			lblCustomInstructions.setText(this.activity.getInstructions());
		}
		
		lblCustomInstructions.setVerticalAlignment(SwingConstants.TOP);
		lblCustomInstructions.setBackground(new Color(255, 255, 255));
		scrollPane.setViewportView(lblCustomInstructions);
		lblCustomInstructions.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		JButton btnCancel = new JButton("Annuller");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(ActivityInstructionsView.this, "Er du sikker på, at du vil annullere denne aktivitet?", "Bekræftelse af annullering", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
					return;
				}
				
				ActivityInstructionsView.this.appController.activitySelection();
			}
		});
		add(btnCancel, "4, 8");
		
		JButton btnContinue = new JButton("Fortsæt");
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ActivityInstructionsView.this.appController.activityStarted();
			}
		});
		add(btnContinue, "6, 8");
		
	}
	
}
