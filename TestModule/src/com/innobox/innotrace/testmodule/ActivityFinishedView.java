/**
 * File:		ActivityFinishedView.java
 * Package:		com.innobox.innotrace.testmodule
 * Project:		TestModule
 * Created:		07/04/2014 19.17.19
 * Author:		jonathanfrank
 * 
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * For further information, please read LICENSE.md
 */
package com.innobox.innotrace.testmodule;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jonathanfrank
 * 
 */
public class ActivityFinishedView extends JPanel {
	private ApplicationController appController;
	
	/**
	 * Create the panel.
	 */
	public ActivityFinishedView(ApplicationController appController) {
		setBackground(Color.WHITE);
		this.appController = appController;
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		
		JLabel lblMessage = new JLabel("Tak for din deltagelse");
		lblMessage.setForeground(new Color(0, 51, 102));
		lblMessage.setFont(new Font("Dialog", Font.BOLD, 24));
		add(lblMessage, "2, 2, center, center");
		
		JButton btnFinish = new JButton("Afslut");
		btnFinish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ActivityFinishedView.this.appController.activitySelection();
			}
		});
		add(btnFinish, "2, 4, right, bottom");
	}
	
}
