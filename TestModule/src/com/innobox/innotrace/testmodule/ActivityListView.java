/**
 * File:	ActivityListView.java
 * Package:	com.innobox.innotrace.testmodule
 * Project:	TestModule
 * Author:	jona4946
 * Created:	31/03/2014 08.57.30
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.testmodule;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import com.innobox.innotrace.data.Activity;
import com.innobox.innotrace.data.DataAccessException;
import com.innobox.innotrace.data.IActivityDAO;
import com.innobox.innotrace.data.IObservationDAO;
import com.innobox.innotrace.data.Observation;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * @author jona4946
 * 
 */
public class ActivityListView extends JPanel {
	
	private ApplicationController appController;
	
	private ActivityTableModel newActivitiesModel;
	private ActivityTableModel previousActivitiesModel;
	private JTable table;
	private JTable table_1;
	
	/**
	 * @param appController
	 */
	public ActivityListView(ApplicationController appController) {
		this.appController = appController;
		newActivitiesModel = new ActivityTableModel();
		previousActivitiesModel = new ActivityTableModel();
		update();
		
		setBackground(new Color(255, 255, 255));
		setLayout(new CardLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, "name_3293118454075");
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Nye aktiviteter", null, panel, null);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 255, 255));
		panel_2.setPreferredSize(new Dimension(200, 10));
		panel_2.setBorder(new TitledBorder(null, "Handlinger", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_2, BorderLayout.EAST);
		panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.GROWING_BUTTON_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.PARAGRAPH_GAP_ROWSPEC, RowSpec.decode("549px"), }));
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedRow() != -1) {
					Activity a = newActivitiesModel.getRowAt(table_1.getSelectedRow());
					ActivityListView.this.appController.activitySelected(a);
				} else {
					ActivityListView.this.appController.showErrorDialog("Du skal vælge en aktivitet først");
				}
			}
		});
		panel_2.add(btnStart, "2, 2");
		
		JButton btnCancel = new JButton("Forkast");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedRow() != -1) {
					Activity a = newActivitiesModel.getRowAt(table_1.getSelectedRow());
					cancel(a);
				} else {
					ActivityListView.this.appController.showErrorDialog("Du skal vælge en aktivitet først");
				}
			}
		});
		panel_2.add(btnCancel, "2, 4");
		
		JButton btnUpdateList = new JButton("Opdater liste");
		btnUpdateList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});
		panel_2.add(btnUpdateList, "2, 6");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBackground(new Color(255, 255, 255));
		panel.add(scrollPane_1, BorderLayout.CENTER);
		
		table_1 = new JTable();
		table_1.setModel(newActivitiesModel);
		scrollPane_1.setViewportView(table_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		tabbedPane.addTab("Tidligere aktiviteter", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(new Color(255, 255, 255));
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(previousActivitiesModel);
		scrollPane.setViewportView(table);
	}
	
	protected void update() {
		try {
			IActivityDAO dao = appController.getDataAccessService().createActivityDAO();
			String currentUsername = appController.getDataAccessService().getCurrentUser().getUsername();
			newActivitiesModel.setActivities(dao.getActivitiesBySubject(currentUsername, false));
			previousActivitiesModel.setActivities(dao.getActivitiesBySubject(currentUsername, true));
		} catch (DataAccessException ex) {
			appController.showErrorDialog("<html><b>Serveren afviste forspørgelsen:</b><br>%s", ex.getMessage());
		} catch (RemoteException ex) {
			appController.showErrorDialog("<html><b>Kunne ikke kommunikere med serveren:</b><br>%s", ex.getMessage());
		}
	}
	
	protected void cancel(Activity a) {
		try {
			IObservationDAO dao = appController.getDataAccessService().createObservationDAO();
			String currentUsername = appController.getDataAccessService().getCurrentUser().getUsername();
			Observation o = new Observation(a.getId(), currentUsername, true, null);
			dao.addObservation(o);
			update();
		} catch (DataAccessException ex) {
			appController.showErrorDialog("<html><b>Serveren afviste forspørgelsen:</b><br>%s", ex.getMessage());
		} catch (RemoteException ex) {
			appController.showErrorDialog("<html><b>Kunne ikke kommunikere med serveren:</b><br>%s", ex.getMessage());
		}
	}
}
