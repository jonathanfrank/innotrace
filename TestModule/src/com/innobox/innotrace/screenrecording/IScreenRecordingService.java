/**
 * File:	IScreenRecordingService.java
 * Package:	com.innobox.innotrace.screenrecording
 * Project:	TestModule
 * Author:	jona4946
 * Created:	20/03/2014 10.59.33
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.screenrecording;

import java.awt.image.BufferedImage;
import java.util.UUID;

/**
 * Defines a common interface for screen recording tools.
 * 
 * @author jona4946
 * 
 */
public interface IScreenRecordingService {
	/**
	 * Returns a unique identification of a screen recording tool
	 * 
	 * @return a unique identification of a screen recording tool
	 */
	public UUID getUUID();

	/**
	 * Identifies the manufacturer of this screen recording tool
	 * 
	 * @return the manufacturer of this screen recording tool
	 */
	public String getManufacturer();

	/**
	 * Identifies the name of this screen recording tool
	 * 
	 * @return the name of this screen recording tool
	 */
	public String getModel();

	/**
	 * Identifies the version of this screen recording tool
	 * 
	 * @return the version of this screen recording tool
	 */
	public String getVersion();

	/**
	 * Obtains a snapshot of the whole framebuffer
	 * 
	 * @return the screen shot
	 */
	public BufferedImage getScreenshot();
}
