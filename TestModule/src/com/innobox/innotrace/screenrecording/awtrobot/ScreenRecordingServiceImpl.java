/**
 * File:	ScreenRecordingServiceImpl.java
 * Package:	com.innobox.innotrace.screenrecording.awtrobot
 * Project:	TestModule
 * Author:	jona4946
 * Created:	20/03/2014 11.13.22
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.screenrecording.awtrobot;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.UUID;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Typed;

import com.innobox.innotrace.screenrecording.IScreenRecordingService;

/**
 * A screen recording tool based on java.awt.robot
 * 
 * @author jona4946
 * 
 */
@Default
@Typed(IScreenRecordingService.class)
public class ScreenRecordingServiceImpl implements IScreenRecordingService {

	private GraphicsDevice monitor;
	private Robot robot;

	public static final int MAX_FRAMERATE = 60;

	/**
	 * Creates a new ScreenRecordingServiceImpl
	 * 
	 * @throws AWTException
	 *             The constructor fails if the system framebuffer is
	 *             unavailable or if read access is disabled for some reason.
	 */
	public ScreenRecordingServiceImpl() throws AWTException {
		this(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());
	}

	/**
	 * Creates a new ScreenRecordingServiceImpl
	 * 
	 * @param monitor
	 *            The screen to be captured
	 * @throws AWTException
	 *             The constructor fails if the system framebuffer is
	 *             unavailable or if read access is disabled for some reason.
	 */
	public ScreenRecordingServiceImpl(GraphicsDevice monitor) throws AWTException {
		this.monitor = monitor;
		robot = new Robot(monitor);
	}

	/**
	 * @see com.innobox.innotrace.screenrecording.IScreenRecordingService#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return UUID.fromString("c06009f0-b018-11e3-a5e2-0800200c9a66");
	}

	/**
	 * @see com.innobox.innotrace.screenrecording.IScreenRecordingService#getManufacturer()
	 */
	@Override
	public String getManufacturer() {
		return "InnoBox";
	}

	/**
	 * @see com.innobox.innotrace.screenrecording.IScreenRecordingService#getModel()
	 */
	@Override
	public String getModel() {
		return "java.awt.Robot Recorder";
	}

	/**
	 * @see com.innobox.innotrace.screenrecording.IScreenRecordingService#getVersion()
	 */
	@Override
	public String getVersion() {
		return "1.0";
	}

	/**
	 * @see com.innobox.innotrace.screenrecording.IScreenRecordingService#getScreenshot()
	 */
	@Override
	public BufferedImage getScreenshot() {
		int x = monitor.getDisplayMode().getWidth();
		int y = monitor.getDisplayMode().getHeight();

		return robot.createScreenCapture(new Rectangle(x, y));
	}

}
