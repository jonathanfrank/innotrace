/**
 * File:	IGazeListener.java
 * Package:	com.innobox.innotrace.gazetracking
 * Project:	TestModule
 * Author:	jona4946
 * Created:	18/03/2014 14.33.26
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.gazetracking;

import java.awt.geom.Point2D;

/**
 * Defines the common interface for objects listening for gaze tracker-related
 * events
 * 
 * @author jona4946
 */
public interface IGazeListener {
	/**
	 * Called when gaze is detected
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void gazeDetected(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when gaze has disappeared
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void gazeDisappeared(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when the gaze position has changed
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 * @param position
	 *            the updated gaze position
	 */
	public void positionChanged(IGazeTrackingService gazeTrackingService, Point2D position);

	/**
	 * Called when the gaze tracker becomes available
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void trackerAvailable(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when the gaze tracker becomes unavailable
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void trackerUnavailable(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when the gaze tracker is started successfully
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void trackerStarted(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when the gaze tracker is stopped
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 */
	public void trackerStopped(IGazeTrackingService gazeTrackingService);

	/**
	 * Called when the calibration state of the gaze tracker changes
	 * 
	 * @param gazeTrackingService
	 *            the gaze tracker, which created this event
	 * @param calibrated
	 *            the updated calibration state (true = calibrated, false =
	 *            needs calibration)
	 */
	public void calibrationStateChanged(IGazeTrackingService gazeTrackingService, boolean calibrated);
}
