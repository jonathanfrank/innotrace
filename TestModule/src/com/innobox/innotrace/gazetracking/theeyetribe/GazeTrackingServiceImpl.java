/**
 * File:	GazeTrackingServiceImpl.java
 * Package:	com.innobox.innotrace.gazetracking.theeyetribe
 * Project:	TestModule
 * Author:	jona4946
 * Created:	18/03/2014 14.58.41
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.gazetracking.theeyetribe;

import java.awt.geom.Point2D;
import java.util.UUID;
import java.util.logging.Logger;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Typed;

import com.innobox.innotrace.gazetracking.AbstractGazeTrackingService;
import com.innobox.innotrace.gazetracking.IGazeTrackingService;
import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.GazeManager.ApiVersion;
import com.theeyetribe.client.GazeManager.ClientMode;
import com.theeyetribe.client.GazeManager.TrackerState;
import com.theeyetribe.client.ICalibrationResultListener;
import com.theeyetribe.client.IGazeListener;
import com.theeyetribe.client.ITrackerStateListener;
import com.theeyetribe.client.data.CalibrationResult;
import com.theeyetribe.client.data.GazeData;

/**
 * An implementation of IGazeTrackingService based on TheEyeTribe.
 * 
 * To use this implementation, the following requirements must be satisfied:
 * <ul>
 * <li>A TheEyeTribe sensor must be connected to the computer using a USB3.0
 * link</li>
 * <li>The server from TheEyeTribe SDK must be installed before this class is
 * loaded</li>
 * </ul>
 * 
 * @author jona4946
 * 
 */
@Default
@Typed(IGazeTrackingService.class)
public class GazeTrackingServiceImpl extends AbstractGazeTrackingService implements IGazeTrackingService, IGazeListener, ITrackerStateListener, ICalibrationResultListener {
	
	private GazeManager gm;
	public static final int NUM_CALIBRATION_POINTS = 9;
	
	/**
	 * Creates a new GazeTrackingServiceImpl
	 */
	public GazeTrackingServiceImpl() {
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#start()
	 */
	@Override
	public void start() {
		if (isRunning())
			return;
		
		gm = GazeManager.getInstance();
		activateTracker(gm);
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (gm == null)
					return;
				
				deactivateTracker(gm);
				gm = null;
				
				Logger.getLogger(getClass().getSimpleName()).info("Gaze Tracker killed");
			}
		});
		
		setRunning(true);
		
		Logger.getLogger(getClass().getSimpleName()).info("Gaze Tracker started");
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#stop()
	 */
	@Override
	public void stop() {
		if (!isRunning())
			return;
		
		deactivateTracker(gm);
		gm = null;
		
		setGazeDetected(false);
		setRunning(false);
		
		Logger.getLogger(getClass().getSimpleName()).info("Gaze Tracker stopped");
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#calibrate()
	 */
	@Override
	public void calibrate() {
		CalibrationHelper ch = new CalibrationHelper(gm, NUM_CALIBRATION_POINTS);
		ch.calibrate();
	}
	
	/**
	 * @see com.theeyetribe.client.ITrackerStateListener#onTrackerStateChanged(int)
	 */
	@Override
	public void onTrackerStateChanged(int trackerState) {
		TrackerState state = TrackerState.fromInt(trackerState);
		
		switch (state) {
		case TRACKER_CONNECTED:
			setAvailable(true);
			break;
		
		default:
			setAvailable(false);
			break;
		}
		
		Logger.getLogger(getClass().getSimpleName()).info(String.format("Tracker state updated (%s)", state.toString()));
	}
	
	/**
	 * @see com.theeyetribe.client.ITrackerStateListener#OnScreenStatesChanged(int,
	 *      int, int, float, float)
	 */
	@Override
	public void OnScreenStatesChanged(int screenIndex, int screenResolutionWidth, int screenResolutionHeight, float screenPhysicalWidth, float screenPhysicalHeight) {
		Logger.getLogger(getClass().getSimpleName()).info(String.format("Screen state changed (SCREEN=%d, RES=%d*%d, PHYSICAL=%d*%d", screenIndex, screenResolutionWidth, screenResolutionHeight, screenPhysicalWidth, screenPhysicalHeight));
		// NO-OP
	}
	
	/**
	 * @see com.theeyetribe.client.IGazeListener#onGazeUpdate(com.theeyetribe.client.data.GazeData)
	 */
	@Override
	public void onGazeUpdate(GazeData gazeData) {
		setLastPosition(new Point2D.Double(gazeData.smoothedCoordinates.x, gazeData.smoothedCoordinates.x));
		Logger.getLogger(getClass().getSimpleName()).info(String.format("Gaze update (%s)", gazeData.toString()));
	}
	
	/**
	 * @see com.theeyetribe.client.ICalibrationResultListener#onCalibrationChanged(boolean,
	 *      com.theeyetribe.client.data.CalibrationResult)
	 */
	@Override
	public void onCalibrationChanged(boolean isCalibrated, CalibrationResult calibResult) {
		setCalibrated(isCalibrated && calibResult.result);
		Logger.getLogger(getClass().getSimpleName()).info(String.format("Gaze tracker calibrated (%s)", calibResult.toString()));
	}
	
	/**
	 * Activates the gaze tracker. This is an internal method, which is not
	 * supposed to be called by other classes. Consequently, it is declared
	 * private.
	 * 
	 * @param gm
	 *            the GazeManager obtained from the TheEyeTribe API
	 */
	private void activateTracker(GazeManager gm) {
		if (!gm.activate(ApiVersion.VERSION_1_0, ClientMode.PUSH)) {
			throw new RuntimeException("Could not connect to TheEyeTribe daemon.");
		}
		
		gm.addGazeListener(this);
		gm.addTrackerStateListener(this);
		gm.addCalibrationResultListener(this);
	}
	
	/**
	 * Deactivates the gaze tracker. This is an internal method, which is not
	 * supposed to be called by other classes. Consequently, it is declared
	 * private.
	 * 
	 * @param gm
	 *            the GazeManager obtained from the TheEyeTribe API
	 */
	private void deactivateTracker(GazeManager gm) {
		gm.removeGazeListener(GazeTrackingServiceImpl.this);
		gm.removeTrackerStateListener(GazeTrackingServiceImpl.this);
		gm.removeCalibrationResultListener(this);
		gm.deactivate();
	}
	
	// --------------------------------------------
	// INFO
	// --------------------------------------------
	
	/**
	 * @see com.innobox.innotrace.gazetracking.AbstractGazeTrackingService#getManufacturer()
	 */
	@Override
	public String getManufacturer() {
		return "The EyeTribe";
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.AbstractGazeTrackingService#getModel()
	 */
	@Override
	public String getModel() {
		return "The EyeTribe Tracker";
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.AbstractGazeTrackingService#getVersion()
	 */
	@Override
	public String getVersion() {
		return "1.0";
	}
	
	/**
	 * @see com.innobox.innotrace.gazetracking.AbstractGazeTrackingService#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return UUID.fromString("bd834540-b017-11e3-a5e2-0800200c9a66");
	}
	
}
