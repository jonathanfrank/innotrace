/**
 * File:	CalibrationHelper.java
 * Package:	com.innobox.innotrace.gazetracking.theeyetribe
 * Project:	TestModule
 * Author:	jona4946
 * Created:	20/03/2014 10.43.08
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.gazetracking.theeyetribe;

import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.ICalibrationProcessHandler;
import com.theeyetribe.client.data.CalibrationResult;

/**
 * A helper class containing methods to calibrate a TheEyeTribe sensor
 * 
 * @author jona4946
 * 
 */
public class CalibrationHelper implements ICalibrationProcessHandler {
	private GazeManager gm;
	private CalibrationResult calibrationResult;
	private int numCalibrationPoints;
	private boolean calibrationFinished;

	/**
	 * Creates a new CalibrationHelper
	 * 
	 * @param gm
	 *            the GazeManager (TheEyeTribe API)
	 */
	public CalibrationHelper(GazeManager gm, int numCalibrationPoints) {
		this.gm = gm;
		this.numCalibrationPoints = numCalibrationPoints;
	}

	/**
	 * Calibrates the sensor. This method blocks the current thread, while the
	 * calibration is running. It cannot be called by the calibration thread,
	 * because a deadlock will occur.
	 * 
	 * @return true if the calibration was done successfully
	 */
	public boolean calibrate() {
		calibrationFinished = false;
		calibrationResult = null;

		gm.calibrationStart(numCalibrationPoints, this);

		while (true) {
			synchronized (this) {
				if (calibrationFinished) {
					break;
				} else {
					try {
						wait();
					} catch (InterruptedException e) {
						// NO-OP
					}
				}
			}
		}

		return calibrationResult.result;
	}

	/**
	 * Calibrates the sensor.
	 * 
	 * @param isBlocking
	 *            whether this method should block the current thread.
	 * @return true if the calibration was done successfully
	 */
	public boolean calibrate(boolean isBlocking) {
		if (isBlocking) {
			return calibrate();
		} else {
			calibrationFinished = false;
			calibrationResult = null;
			gm.calibrationStart(numCalibrationPoints, this);
			return false;
		}
	}

	/**
	 * @see com.theeyetribe.client.ICalibrationProcessHandler#onCalibrationStarted()
	 */
	@Override
	public void onCalibrationStarted() {
		// NO-OP

	}

	/**
	 * @see com.theeyetribe.client.ICalibrationProcessHandler#onCalibrationProgress(double)
	 */
	@Override
	public void onCalibrationProgress(double progress) {
		// NO-OP
	}

	/**
	 * @see com.theeyetribe.client.ICalibrationProcessHandler#onCalibrationProcessing()
	 */
	@Override
	public void onCalibrationProcessing() {
		// NO-OP
	}

	/**
	 * @see com.theeyetribe.client.ICalibrationProcessHandler#onCalibrationResult(com.theeyetribe.client.data.CalibrationResult)
	 */
	@Override
	public void onCalibrationResult(CalibrationResult calibResult) {
		calibrationResult = calibResult;

		synchronized (this) {
			calibrationFinished = true;
			notifyAll();
		}
	}

}
