/**
 * File:	AbstractGazeTrackingService.java
 * Package:	com.innobox.innotrace.gazetracking
 * Project:	TestModule
 * Author:	jona4946
 * Created:	18/03/2014 14.41.10
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.gazetracking;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Implements some general methods defined in IGazeTrackingService to avoid
 * repeated boilerplate code
 * 
 * @author jona4946
 */
public abstract class AbstractGazeTrackingService implements IGazeTrackingService {
	private ArrayList<IGazeListener> gazeListeners;
	private boolean available;
	private boolean running;
	private boolean gazeDetected;
	private boolean calibrated;
	private Point2D lastPosition;

	/**
	 * Creates a new AbstractGazeTrackingService
	 */
	public AbstractGazeTrackingService() {
		gazeListeners = new ArrayList<>();
		available = false;
		running = false;
		gazeDetected = false;
		lastPosition = null;
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return UUID.fromString("'00000000-0000-0000-0000-000000000000");
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getManufacturer()
	 */
	@Override
	public String getManufacturer() {
		return "Unknown";
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getModel()
	 */
	@Override
	public String getModel() {
		return "Unknown";
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getVersion()
	 */
	@Override
	public String getVersion() {
		return "Unknown";
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#addGazeListener(com.innobox.innotrace.gazetracking.IGazeListener)
	 */
	@Override
	public void addGazeListener(IGazeListener gl) {
		gazeListeners.add(gl);
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#removeGazeListener(com.innobox.innotrace.gazetracking.IGazeListener)
	 */
	@Override
	public void removeGazeListener(IGazeListener gl) {
		gazeListeners.remove(gl);
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getGazeListeners()
	 */
	@Override
	public List<IGazeListener> getGazeListeners() {
		return new ArrayList<>(gazeListeners);
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#isAvailable()
	 */
	@Override
	public boolean isAvailable() {
		return available;
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#isRunning()
	 */
	@Override
	public boolean isRunning() {
		return running;
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#isCalibrated()
	 */
	@Override
	public boolean isCalibrated() {
		return calibrated;
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#isGazeDetected()
	 */
	@Override
	public boolean isGazeDetected() {
		return gazeDetected;
	}

	/**
	 * @see com.innobox.innotrace.gazetracking.IGazeTrackingService#getLastPosition()
	 */
	@Override
	public Point2D getLastPosition() {
		return lastPosition;
	}

	/**
	 * Updates the availability of the gaze tracker
	 * 
	 * @param available
	 *            the availability of the gaze tracker (true = available, false
	 *            = not available)
	 */
	protected void setAvailable(boolean available) {
		this.available = available;

		for (IGazeListener gl : gazeListeners) {
			if (available) {
				gl.trackerAvailable(this);
			} else {
				gl.trackerUnavailable(this);
			}
		}
	}

	/**
	 * Changes the state of this gaze tracker and notifies registered
	 * IGazeListeners
	 * 
	 * @param running
	 *            the new state (true = running, false = stopped)
	 */
	protected void setRunning(boolean running) {
		this.running = running;

		for (IGazeListener gl : gazeListeners) {
			if (running) {
				gl.trackerStarted(this);
			} else {
				gl.trackerStopped(this);
			}
		}
	}

	/**
	 * Sets whether gaze is detected and notifies registered IGazeListeners
	 * 
	 * @param gazeDetected
	 *            whether gaze is detected (true = detected, false = not
	 *            detected)
	 */
	protected void setGazeDetected(boolean gazeDetected) {
		this.gazeDetected = gazeDetected;

		for (IGazeListener gl : gazeListeners) {
			if (gazeDetected) {
				gl.gazeDetected(this);
			} else {
				gl.gazeDisappeared(this);
			}
		}
	}

	/**
	 * Changes the calibration status of this gaze tracker
	 * 
	 * @param calibrated
	 *            the updated calibration status (true = calibrated, false =
	 *            needs calibration)
	 */
	protected void setCalibrated(boolean calibrated) {
		this.calibrated = calibrated;

		for (IGazeListener gl : gazeListeners) {
			gl.calibrationStateChanged(this, calibrated);
		}
	}

	/**
	 * Updates the last known gaze position
	 * 
	 * @param lastPosition
	 *            the last known gaze position
	 */
	protected void setLastPosition(Point2D lastPosition) {
		this.lastPosition = lastPosition;

		for (IGazeListener gl : gazeListeners) {
			gl.positionChanged(this, lastPosition);
		}
	}
}
