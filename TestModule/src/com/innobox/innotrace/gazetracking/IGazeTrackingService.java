/**
 * File:	IGazeTrackingService.java
 * Package:	com.innobox.innotrace.gazetracking
 * Project:	TestModule
 * Author:	jona4946
 * Created:	18/03/2014 14.30.09
 *
 * Copyright 2014 InnoBox / Jonathan Frank. All rights reserved.
 * Read /license.md for further information.
 */
package com.innobox.innotrace.gazetracking;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.UUID;

/**
 * Defines a common interface for gaze trackers
 * 
 * @author jona4946
 * 
 */
public interface IGazeTrackingService {
	/**
	 * Identifies this gaze tracker uniquely
	 * 
	 * @return the unique ID of this gaze tracker
	 */
	public UUID getUUID();

	/**
	 * Identifies the manufacturer of the gaze tracker driver and/or the
	 * hardware
	 * 
	 * @return The name of the manufacturer
	 */
	public String getManufacturer();

	/**
	 * Identifies the name of the hardware controlled by the gaze tracker driver
	 * 
	 * @return The name of the gaze tracker hardware
	 */
	public String getModel();

	/**
	 * Identifies the version of the gaze tracker driver and/or hardware
	 * 
	 * @return The gaze tracker version
	 */
	public String getVersion();

	/**
	 * Registers an IGazeListener
	 * 
	 * @param gl
	 *            the IGazeListener
	 */
	public void addGazeListener(IGazeListener gl);

	/**
	 * Unregisters an IGazeListener
	 * 
	 * @param gl
	 */
	public void removeGazeListener(IGazeListener gl);

	/**
	 * Returns a list of registered IGazeListeners
	 * 
	 * @return the list of registered IGazeListeners
	 */
	public List<IGazeListener> getGazeListeners();

	/**
	 * Returns the availability of the gaze tracker
	 * 
	 * @return the availability of the gaze tracker (true = available, false =
	 *         not available)
	 */
	public boolean isAvailable();

	/**
	 * Starts the gaze tracker. Afterwards events will be reported to registered
	 * IGazeListeners.
	 */
	public void start();

	/**
	 * Stops the gaze tracker. Afterwards no events will be reported to any
	 * GazeListener.
	 */
	public void stop();

	/**
	 * Calibrates the gaze tracker. During the calibration process, this method
	 * will block the current thread.
	 */
	public void calibrate();

	/**
	 * Returns the state of the gaze listener
	 * 
	 * @return the state of the gaze listener (true = running, false = stopped)
	 */
	public boolean isRunning();

	/**
	 * Returns the calibration state of the gaze listener
	 * 
	 * @return the calibration state of the gaze listener (true = calibrated,
	 *         false = needs calibration)
	 */
	public boolean isCalibrated();

	/**
	 * Returns whether gaze is detected and notifies registered IGazeListeners
	 * 
	 * @return whether gaze is detected (true = detected, false = not detected)
	 */
	public boolean isGazeDetected();

	/**
	 * Returns the last known gaze position
	 * 
	 * @return the last known gaze position
	 */
	public Point2D getLastPosition();
}
